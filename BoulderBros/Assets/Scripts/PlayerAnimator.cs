using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Unity.Netcode;

public class PlayerAnimator : NetworkBehaviour
{
    [Header("References/Managers")]
    [SerializeField] private PlayerController playerController;
    [SerializeField] private Animator playerAnimator;
    [SerializeField] private PlayerEffectManager playerEffectManager;
    [SerializeField] private SpriteRenderer[] sprites;
    [SerializeField] private GameObject bbqStick;
    
    [Header("References/Animation Clips")]
    [SerializeField] private AnimationClip runAnim;
    [SerializeField] private AnimationClip idleAnim;
    [SerializeField] private AnimationClip jumpStartAnim;
    [SerializeField] private AnimationClip jumpIdleAnim;
    [SerializeField] private AnimationClip jumpLandAnim;
    [SerializeField] private AnimationClip onRopeIdleAnim;
    
    [Header("Face Animation")]
    [SerializeField] private SpriteRenderer[] faceExpressionRenderers;
    [SerializeField] private GameObject[] faceExpressions;
    [SerializeField] private Gradient faceRedGradient;
    [SerializeField] private SpriteRenderer head;
    
    [Header("Cold Breath Stuff")]
    [SerializeField] private GameObject coldBreath;
    [SerializeField] private Animator coldBreathAnimator;
    
    [Header("Impact Dust Stuff")]
    [SerializeField] private ParticleSystem impactDust;
    [SerializeField] private int impactDustAmount;
    
    [Header("Leave Particle Stuff")]
    [SerializeField] private ParticleSystem leavePS;
    [SerializeField] private int leaveEmitAmount;
    
    [Header("Climb Sorting Layer change")]
    [SerializeField] private SpriteRenderer backLeg;
    [SerializeField] private SpriteRenderer backArm;
    private int initialBackLegOrder;
    private int initialBackArmOrder;
    [Space(5)]
    
    [Header("On Rope Stuff")]
    [SerializeField] private float onRopeIdleAnimBaseSpeed = 1f;
    [SerializeField] private float onRopeIdleSpeedMultiplier = 0.2f;

    // Start is called before the first frame update

    private void Start()
    {
        initialBackArmOrder = backArm.sortingOrder;
        initialBackLegOrder = backLeg.sortingOrder;

        coldBreathAnimator = coldBreath.GetComponent<Animator>();
    }

    private void Update()
    {
        //if its online multiplayer and not the owner, dont do shit
        if(NetworkController.Instance.onlineMultiplayer && !IsOwner) return;
        
        UpdateSlideAndDrag(playerController.isSliding, playerController.isDragged);
        UpdateMovementAnimation(playerController.isMovingX, playerController.isMovingY);
    }
    
    private void FixedUpdate()
    {
        //if its online multiplayer and not the owner, dont do shit
        if(NetworkController.Instance.onlineMultiplayer && !IsOwner) return;
        
        if(playerController.isOnRope)
            UpdateRopeHangIdleSpeed();
    }

    #region General
    public void FlipSprites()
    {
        foreach (SpriteRenderer render in sprites)
        {
            if (!render.flipX)
            {
                render.flipX = true;
            }
        }
        
        foreach (SpriteRenderer render in faceExpressionRenderers)
        {
            if (!render.flipX)
            {
                render.flipX = true;
            }
        }
    }

    public void ChangeSortingOrder(SpriteRenderer render)
    {
        backArm.sortingOrder = render.sortingOrder - 1;
        backLeg.sortingOrder = render.sortingOrder - 1;
    }

    public void ResetSortingOrder()
    {
        backArm.sortingOrder = initialBackArmOrder;
        backLeg.sortingOrder = initialBackLegOrder;
    }

    #endregion
    
    #region Face
    public void SetFaceExpression(string expressionName)
    {
        foreach (GameObject obj in faceExpressions)
        {
            obj.SetActive(false);

            if (obj.name.Equals(expressionName))
                obj.SetActive(true);
        }
    }

    public void ShouldWowFace(bool wow)
    {
        if (wow)
        {
            SetFaceExpression("wow");
        }
        else
        {
            SetFaceExpression("happy");
        }
    }

    public void UpdateFaceColor(float value)
    {
        head.color = faceRedGradient.Evaluate(1 - value/100f);   
    }

    public void UpdateFaceExpression(float value, bool wow)
    {
        if(value <= 10)
        {
            SetFaceExpression("hurt2");
                playerEffectManager.SetColdBreath(true);
            return;
        }
        if (value <= 20)
        {
            SetFaceExpression("hurt");
                playerEffectManager.SetColdBreath(true);
            return;
        }
        if (value <= 40)
        {
            SetFaceExpression("mad");
                playerEffectManager.SetColdBreath(false);
            return;
        }
        if (value <= 60)
        {
            SetFaceExpression("unhappy");
            playerEffectManager.SetColdBreath(false);
            return;
        }
        if (value <= 75)
        {
            SetFaceExpression("annoyed");
            playerEffectManager.SetColdBreath(false);
            return;
        }
        if (value > 75)
        {
            ShouldWowFace(wow);
            playerEffectManager.SetColdBreath(false);
            return;
        }
    }

    // public void ResetFaceRed()
    // {
    //     faceRedGradient.Evaluate(0);
    // }
    
    #endregion
    
    #region Movement States
    public void SetGroundedAnimation(bool grounded)
    {
        if (playerAnimator.GetBool("grounded") != grounded)
        {
            playerAnimator.SetBool("grounded", grounded);
            if(!playerController.isClimbing && grounded)
                playerEffectManager.EmitGroundParticlesDelayed(0.05f);
        }
    }

    public void SetRunAnimation(bool run)
    {
        playerAnimator.SetBool("run", run);
        playerEffectManager.ToggleGroundParticles();
    }

    public void SetExhaustAnimation(bool exhausted)
    {
        playerAnimator.SetBool("exhausted", exhausted);
    }
    private void UpdateMovementAnimation(bool moveX, bool moveY)
    {
        playerAnimator.SetBool("movingX", moveX);
        playerAnimator.SetBool("movingY", moveY);
    }

    public void UpdateTransformSpeeds(float transformSpeedX, float transformSpeedY)
    {
        playerAnimator.SetFloat("transformSpeedX", transformSpeedX);
        playerAnimator.SetFloat("transformSpeedY", transformSpeedY);
    }

    private void UpdateSlideAndDrag(bool isSliding, bool beingDragged)
    {
        playerAnimator.SetBool("isSliding", isSliding);
        playerAnimator.SetBool("beingDragged", beingDragged);
    }
    
    public void JumpAnimation(bool grounded,bool jumped)
    {
        playerEffectManager.EmitGroundParticles();
        playerAnimator.SetBool("grounded", grounded);
        playerAnimator.SetBool("jump", jumped);
        Invoke(nameof(EndJumpAnimation), 0.5f);
    }

    public void SetClimbAnimation(bool isClimbing)
    {
        playerAnimator.SetBool("climbing", isClimbing);
    }
    
    private void EndJumpAnimation()
    {
        playerAnimator.SetBool("jump", false);
    }

    public void SetLedgeJumpAnimation(bool ledgeJump)
    {
        playerAnimator.SetBool("ledgeJump", ledgeJump);
    }

    public void StartPlayerMainMenuAnimation(bool toggle)
    {
        playerAnimator.SetBool("mainMenu", toggle);
        bbqStick.SetActive(toggle);
    }

    public void SetFacingWallAnimation(bool facingWall)
    {
        playerAnimator.SetBool("facingWall", facingWall);
    }
    
    public void SetDeathAnimation(bool death)
    {
        playerAnimator.SetBool("death", death);
    }
    
    #endregion
    
    #region rope Animation States
    
    public void ToggleRopeAnimation()
    {
        playerAnimator.SetTrigger("toggleRope");
    }

    public void ToggleGrabRopeAnimation(bool isOnGround)
    {
        if(!isOnGround)
            playerAnimator.SetTrigger("hangingOnRopeIdle");
    }

    //normal speed of 1 will be multiplied with this value
    private void SetRopeHangIdleSpeed(float speed)
    {
        playerAnimator.SetFloat("hangingOnRopeSpeedMultiplier", speed);
    }

    private void SetXVelocity(float speed)
    {
        playerAnimator.SetFloat("xVelocity", speed);
    }

    private void UpdateRopeHangIdleSpeed()
    {
        Vector2 playerVelocity = playerController.GetPlayerMovement().GetRigidbodyVelocity();
        float factor = Mathf.Abs(playerVelocity.x * onRopeIdleSpeedMultiplier);
        SetXVelocity(playerVelocity.x);
        SetRopeHangIdleSpeed(onRopeIdleAnimBaseSpeed + factor);
    }
    
    #endregion
}
