using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSwitchTrigger : VolumeTrigger
{
    new private void OnTriggerExit2D(Collider2D other)
    {
        if(other.gameObject.name == "CameraMidPoint")
            CameraManager.Instance.CheckCameraPosition(transform.position.x);
    }
}
