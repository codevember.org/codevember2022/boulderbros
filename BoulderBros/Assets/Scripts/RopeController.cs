using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RopeController : MonoBehaviour
{
    public enum RopeType
    {
        dynamic,
        attached
    }
    
    [Header("RopeType")]
    [SerializeField] private RopeType ropeType;
    [SerializeField] private bool isElastic = false;

    [Header("RopeLength etc.")]
    [SerializeField] private float currentRopeLength;
    [SerializeField] private float currentRopeSegmentLength;
    [SerializeField] private float maxRopeLength;
    [SerializeField] private float minRopeLength;
    [SerializeField] private float ropeSizeStep = 0.25f;
    [SerializeField] private float ropeSegmentRendererGapFix = 1.1f;

    [Header("Rope Attachment to player")]
    [SerializeField] private float playerAttachOffset = 0.25f;
    [SerializeField] private float attachRopeMaxDistance;
    [SerializeField] private float currentPlayerToRopeDistance;
    [SerializeField] private PlayerController currentStartRopePlayer;
    [SerializeField] private PlayerController currentEndRopePlayer;

    [Header("References")]
    [SerializeField] private Rigidbody2D ropeControllerAnchorRigid;
    [SerializeField] private Transform playerMidTransform;
    [SerializeField] private GameObject rope;
    [SerializeField] private GameObject ropePrefab;
    [SerializeField] private RopeSegment startSegment;
    [SerializeField] private HingeJoint2D midJoint;
    [SerializeField] private RopeSegment endSegment;
    [SerializeField] private GameObject endJointObject;
    [SerializeField] private RopeSegment[] ropeSegments;
    // Start is called before the first frame update


    private void Awake()
    {
        ropeControllerAnchorRigid = gameObject.GetComponent<Rigidbody2D>();
    }

    private void Start()
    {
        SetRopeLength(currentRopeLength);
    }

    private void OnValidate()
    {
        SetRopeLength(currentRopeLength);
    }

    public RopeType GetRopeType()
    {
        return ropeType;
    }

    public Rigidbody2D GetRopeStartRigidbody()
    {
        return startSegment.gameObject.GetComponent<Rigidbody2D>();
    }

    public Rigidbody2D GetRopeEndRigidBody()
    {
        return endSegment.gameObject.GetComponent<Rigidbody2D>();
    }

    public Rigidbody2D GetRopeAnchorRigidBody()
    {
        return ropeControllerAnchorRigid;
    }

    public float GetCurrentRopeLength()
    {
        return currentRopeLength;
    }

    public float GetCurrentRopeSegmentLength()
    {
        return currentRopeSegmentLength;
    }

    public float GetPlayerAttachOffset()
    {
        return playerAttachOffset;
    }

    public HingeJoint2D GetMidJoint()
    {
        return midJoint;
    }

    public bool GetBothPlayersOnSameRope()
    {
        return (currentEndRopePlayer && currentStartRopePlayer);
    }

    //If player toggling player is on Rope already - detach him otherwise check for distance to rope ends and attach
    public void ToggleRope(PlayerController controller)
    {
        PlayerRope currentPlayerRopeComponent = controller.GetPlayerRope();
        if(currentStartRopePlayer == controller)
        {
            currentPlayerRopeComponent.SetIsOnRope(false);
            DetachRope(controller);
            return;
        }
        if(currentEndRopePlayer == controller)
        {
            currentPlayerRopeComponent.SetIsOnRope(false);
            DetachRope(controller);  
            return;
        }

        float disToStart = Vector3.Distance(controller.transform.position, startSegment.transform.position);
        float disToEnd = Vector3.Distance(controller.transform.position, endSegment.transform.position);
        if(currentStartRopePlayer is null)
        {
            if (disToStart <= disToEnd)
            {
                if (disToStart <= attachRopeMaxDistance)
                {
                    currentStartRopePlayer = controller;
                    currentPlayerRopeComponent.SetIsOnRope(true);
                    AttachRope(controller, startSegment.getSegmentHingeJoint());
                    //controller.ToggleDistanceJoint(true);
                }
                return;
            }
        }

        if (currentEndRopePlayer is null)
        {
            if (disToEnd <= disToStart)
            {
                if (disToEnd <= attachRopeMaxDistance)
                {
                    currentEndRopePlayer = controller;
                    currentPlayerRopeComponent.SetIsOnRope(true);
                    AttachRope(controller, endSegment.GetPlayerEndHingeJoint());
                    //controller.ToggleDistanceJoint(true); 
                }
                return;
            }
        }
    }    

    public void AttachRope(PlayerController controller, HingeJoint2D targetJoint)
    {
        //rope.transform.position = new Vector3(playerMidTransform.position.x, rope.transform.position.y, rope.transform.position.z);
        if(currentPlayerToRopeDistance <= attachRopeMaxDistance)
        {
            targetJoint.connectedBody = controller.GetComponent<Rigidbody2D>();
            targetJoint.enabled = true;
        } 
    }

    public void DetachRope(PlayerController controller)
    {
        if (controller == currentStartRopePlayer)
        {
            startSegment.GetPlayerEndHingeJoint().enabled = false;
            currentStartRopePlayer = null;
        }
            
        if (controller == currentEndRopePlayer)
        {
            endSegment.GetPlayerEndHingeJoint().enabled = false;
            currentEndRopePlayer = null;
        }
            
        GameManager.Instance.playerController1.GetPlayerRope().ToggleDistanceJoint(true);
        GameManager.Instance.playerController2.GetPlayerRope().ToggleDistanceJoint(true);
    }

    public void DetachRope()
    {
        startSegment.enabled = false;
        endSegment.enabled = false;
        GameManager.Instance.playerController1.GetPlayerRope().ToggleDistanceJoint(true);
        GameManager.Instance.playerController2.GetPlayerRope().ToggleDistanceJoint(true);

        //rope.SetActive(false);
    }
    public void IncreaseRopeLength()
    {
        if (isElastic)
        {
            if (currentRopeLength < maxRopeLength)
            {
                ChangeRopeLength(ropeSizeStep);
                SetRopeLength(currentRopeLength);

                //foreach (RopeSegment segment in ropeSegments)
                //{
                //    segment.IncreaseSegmentSize(ropeSizeStep);
                //}
            }
            else
            {
                Debug.Log("Cant make rope any longer");
            }
        }
        else
        {
            Debug.Log("Rope not stretchable");
        }
    }

    public void DecreaseRopeLength()
    {
        if (isElastic)
        {
            if (currentRopeLength > minRopeLength)
            {
                if (GameManager.Instance.GetPlayerManager().GetCurrentPlayerDistance() < (currentRopeLength - ropeSizeStep))
                {
                    ChangeRopeLength(-ropeSizeStep);
                    SetRopeLength(currentRopeLength);

                    //foreach (RopeSegment segment in ropeSegments)
                    //{
                    //    segment.DecreaseSegmentSize(ropeSizeStep);
                    //}
                }
                else
                    Debug.Log("Players not close enough to shorten rope");
            }
            else
            {
                Debug.Log("Cant make rope any shorter");
            }
        }
        else
        {
            Debug.Log("Rope not stretchable");
        } 
    }
    
    public void SetRopeLength(float value)
    {
        currentRopeLength = value;
        UpdateRopeSegmentLength();

        foreach (RopeSegment segment in ropeSegments)
        {
            segment.SetRopeRendererGapFix(ropeSegmentRendererGapFix);
            segment.SetSegmentSize(currentRopeSegmentLength);
        }
    }

    private void ChangeRopeLength(float value)
    {
        currentRopeLength += value;
        UpdateRopeSegmentLength();
    }

    private void UpdateRopeSegmentLength()
    {
        currentRopeSegmentLength = currentRopeLength / ropeSegments.Length;
    }

    public void SetPlayer1Connection(Rigidbody2D rigid)
    {
        startSegment.GetPlayerEndHingeJoint().connectedBody = rigid;
    }

    public void SetPlayer2Connection(Rigidbody2D rigid)
    {
        endSegment.GetPlayerEndHingeJoint().connectedBody = rigid;
    }
}
