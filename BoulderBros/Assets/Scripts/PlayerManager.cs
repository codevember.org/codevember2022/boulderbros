// using System.Collections;
// using System.Collections.Generic;

using System;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    [Header("References")] 
    [SerializeField] private PlayerController playerController1;
    [SerializeField] private PlayerController playerController2;
    
    [Header("Stats")]
    [SerializeField] private bool aliveP1 = true;
    [SerializeField] private bool aliveP2 = true;
    [SerializeField] private float currentPlayersDistance;
    
    [Header("Positions")]
    [SerializeField] private Vector3 initialPositionP1;
    [SerializeField] private Vector3 initialPositionP2;
    [SerializeField] private Vector3 newGamePositionP1;
    [SerializeField] private Vector3 newGamePositionP2;
    
    private void Start()
    {
        initialPositionP1 = playerController1.transform.position;
        initialPositionP2 = playerController2.transform.position;
        
    }

    private void FixedUpdate()
    {
        UpdateCurrentPlayersDistance();
    }
    
    public void SetPlayerAlive(PlayerController controller, bool value)
    {
        if (controller == playerController1)
            aliveP1 = value;
        if (controller == playerController2)
            aliveP2 = value;
    }

    public void SetPlayersAlive(bool value)
    {
        aliveP1 = value;
        aliveP2 = value;
    }

    public bool GetPlayerAlive(PlayerController controller)
    {
        if(controller == playerController1)
            return aliveP1;
        else if (controller == playerController2)
            return aliveP2;
        else
            return false;      
    }

    public void ResetPlayers()
    {
        EnablePlayers();
        SetPlayersAlive(true);
        playerController1.ResetPlayer();
        playerController2.ResetPlayer();
    }

    public void ResetPlayersToStart()
    {
        ResetPlayers();
        ResetPlayerPositionsToStart();
        playerController1.GetPlayerMovement().Flip();
        playerController2.GetPlayerMovement().Flip();
        playerController1.GetPlayerAnimator().StartPlayerMainMenuAnimation(false);
        playerController2.GetPlayerAnimator().StartPlayerMainMenuAnimation(false);
    }

    private void EnablePlayers()
    {
        playerController1.enabled = true;
        playerController2.enabled = true;
    }
    public void DisablePlayers()
    {
        playerController1.ResetPlayerToStart();
        playerController2.ResetPlayerToStart();
    }

    public void SetPlayersInputEnabled(bool setter)
    {
        playerController1.GetPlayerInput().SetPlayerInputEnabled(setter);
        playerController2.GetPlayerInput().SetPlayerInputEnabled(setter);
    }

    public void ResetPlayerPositions(Vector3 posP1, Vector3 posP2)
    {
        playerController1.transform.position = posP1;
        playerController2.transform.position = posP2;
    }

    public void ResetPlayerPositionsToStart()
    {
        playerController1.transform.position = newGamePositionP1;
        playerController2.transform.position = newGamePositionP2;
    }

    private void UpdateCurrentPlayersDistance()
    {
        currentPlayersDistance = Vector3.Distance(playerController1.transform.position, playerController2.transform.position);
    }
    public float GetCurrentPlayerDistance()
    {
        return currentPlayersDistance;
    }
    
    #region Getters
    
    public PlayerController GetPlayerController1()
    {
        return playerController1;
    }

    public PlayerController GetPlayerController2()
    {
        return playerController2;
    }
    #endregion
}
