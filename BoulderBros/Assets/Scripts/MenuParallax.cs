//using System.Collections;
//using System.Collections.Generic;
using UnityEngine;

public class MenuParallax : MonoBehaviour
{
    [SerializeField] private float offSetMultiplier = 1f;
    [SerializeField] private float smoothTime = .3f;
    [SerializeField] private bool parallaxOnlyY;

    [SerializeField] private Vector3 startPos;
    [SerializeField] private Vector3 velocity;

    [SerializeField] private Vector2 player1CameraInput;

    // Start is called before the first frame update
    void Start()
    {
        startPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        player1CameraInput = GameManager.Instance.playerController1.GetPlayerInput().GetCameraInput();
        if (MenuParallaxManager.Instance.invertInput)
            player1CameraInput = -player1CameraInput;

        var offset = parallaxOnlyY ? new Vector3(1, player1CameraInput.y, 1) : new Vector3(player1CameraInput.x, player1CameraInput.y, 1);

        transform.position = Vector3.SmoothDamp(transform.position, startPos + (offset * offSetMultiplier), ref velocity, smoothTime);
    }

    public void SetOffSetMultiplier(float intensity)
    {
        offSetMultiplier = intensity;
    }

    public void SetParallaxOnlyY(bool set)
    {
        parallaxOnlyY = set;
    }
}
