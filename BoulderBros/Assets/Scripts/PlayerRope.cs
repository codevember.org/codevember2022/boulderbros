using System;
using UnityEngine;

public class PlayerRope : MonoBehaviour
{
	[SerializeField] private PlayerController playerController;
	[SerializeField] private PlayerController otherPlayer;
	[SerializeField] private PlayerAnimator playerAnimator;
	[SerializeField] private PlayerInput playerInput;
	[SerializeField] private DistanceJoint2D playerDistanceJoint;
	[SerializeField] private GameObject currentRope;
	[SerializeField] private RopeController currentRopeController;

	private void Start()
	{
		playerAnimator = playerController.GetPlayerAnimator();
		playerInput = playerController.GetPlayerInput();
		otherPlayer = playerController.GetOtherPlayer();
		
		ClearRopeController();
		playerInput.OnAttachRope += ToggleRope;
		playerInput.OnExtendRope += ExtendRope;
		playerInput.OnShortenRope += ShortenRope;
	}

	#region Rope

	public void SetIsOnRope(bool setter)
    {
		playerController.isOnRope = setter;
		playerAnimator.ToggleGrabRopeAnimation(playerController.isGrounded);
	}

	private RopeController.RopeType CheckRopeType()
    {
		if (currentRopeController.GetRopeType() == RopeController.RopeType.attached)
		{
			return RopeController.RopeType.attached;
		}
		else
			return RopeController.RopeType.dynamic;
    }

	private GameObject GetClosestRope()
	{
		GameObject temp = currentRope;
		float dist = 0;
		float currentSmallest = 0;
		foreach (RopeController rC in GameManager.Instance.GetRopeManager().GetRopeControllers())
		{
			if (rC != null)
			{
				dist = Vector3.Distance(rC.GetMidJoint().transform.position, gameObject.transform.position);

				if (currentSmallest == 0 || dist < currentSmallest)
				{
					temp = rC.gameObject;
					currentSmallest = dist;
				}
			}
		}
		return temp;
	}

	public void SetRope(GameObject ropeObject)
    {
		currentRope = ropeObject;
		currentRopeController = currentRope.GetComponent<RopeController>();
    }

	//Attach/Detach Rope
	private void ToggleRope()
	{
		SetRope(GetClosestRope());
		currentRopeController.ToggleRope(playerController);
		ToggleDistanceJoint(!playerController.isOnRope);
		ClearRopeController();
		playerAnimator.ToggleRopeAnimation();
	}

	//!toggle because current status (is already on rope) is being checked
	public void ToggleDistanceJoint(bool toggle)
    {
		if(currentRopeController is not null)
        {
			if (currentRopeController.GetRopeType() == RopeController.RopeType.attached)
			{
				playerDistanceJoint.enabled = !toggle;
				if (!toggle)
                {
					SetDistanceJointConnected(currentRopeController.GetRopeAnchorRigidBody(), playerDistanceJoint, 0);
					SetMaxPlayerDistance(currentRopeController.GetCurrentRopeLength());
				}
					
				return;
			}
			//if both players on rope enable both distance joints on players and set the correct rigidbody as anchor, 
			//also set connected anchor point to roughly belt of character
			if (currentRopeController.GetBothPlayersOnSameRope())
			{
				EnableBothPlayerDistanceJoints();
				playerController.isOnRopeWithOtherPlayer = true;
				otherPlayer.isOnRopeWithOtherPlayer = true;
				Debug.Log("Both players on same rope should switch connected anchor aswell");
			}
			else
			{
				SetDistanceJointConnectedAnchor(playerDistanceJoint, 0);
				playerController.isOnRopeWithOtherPlayer = false;
				otherPlayer.isOnRopeWithOtherPlayer = false;
				playerDistanceJoint.enabled = false;	
			}
		}
	}

	public void EnableBothPlayerDistanceJoints()
	{
		PlayerRope otherPlayerRope = otherPlayer.GetPlayerRope();
		DistanceJoint2D otherPlayerDistanceJoint = otherPlayer.GetPlayerRope().GetDistanceJoint2D();
		float currentRopeLength = currentRopeController.GetCurrentRopeLength();
		SetMaxPlayerDistance(currentRopeLength);
		otherPlayerRope.SetMaxPlayerDistance(currentRopeLength);
		SetDistanceJointConnected(otherPlayer.GetRigidbody2D(), playerDistanceJoint, currentRopeController.GetPlayerAttachOffset());
		otherPlayerRope.SetDistanceJointConnected(playerController.GetRigidbody2D(), otherPlayerDistanceJoint, currentRopeController.GetPlayerAttachOffset());
		playerDistanceJoint.enabled = otherPlayerDistanceJoint.enabled = true;
	}

	public void SetMaxPlayerDistance(float distance)
	{
		playerDistanceJoint.distance = distance;
		playerController.GetPlayerStats().SetCurrentMaxDistance(distance);
	}

	public void SetDistanceJointConnected(Rigidbody2D rigid, DistanceJoint2D joint, float offset)
    {
		playerDistanceJoint.connectedBody = rigid;
		SetDistanceJointConnectedAnchor(joint, offset);
	}

	public void SetDistanceJointConnectedAnchor(DistanceJoint2D joint, float offset)
    {
		joint.connectedAnchor = new Vector2(joint.connectedAnchor.x, offset);
    }

	private void ClearRopeController()
    {
        if (!playerController.isOnRope)
        {
			currentRopeController = null;
        }
    }

	public bool OnRope()
	{
		if (!playerController.jumped && !playerController.isGrounded && !playerController.isClimbing)
			return true;
		return false;
	}

	public bool BroOnRope()
    {
		if (playerController.isClimbing && otherPlayer.GetPlayerRope().OnRope())
		{
			return true;
		}
		else
			return false;
    }
	
	private void ExtendRope()
	{
		if (currentRopeController is not null)
		{
			playerAnimator.ToggleRopeAnimation();
			currentRopeController.IncreaseRopeLength();

			float newLength = currentRopeController.GetCurrentRopeLength();
			SetMaxPlayerDistance(newLength);
			if(currentRopeController.GetBothPlayersOnSameRope())
				otherPlayer.GetPlayerRope().SetMaxPlayerDistance(newLength);
			Debug.Log(gameObject + "Should extend Rope length");
		}
		
	}

	private void ShortenRope()
	{
		if (currentRopeController is not null)
		{
			playerAnimator.ToggleRopeAnimation();
			currentRopeController.DecreaseRopeLength();

			float newLength = currentRopeController.GetCurrentRopeLength();
			SetMaxPlayerDistance(newLength);
			if (currentRopeController.GetBothPlayersOnSameRope())
				otherPlayer.GetPlayerRope().SetMaxPlayerDistance(newLength);
			Debug.Log(gameObject + "Should decrease Rope length");
		}
	}

	public DistanceJoint2D GetDistanceJoint2D()
	{
		return playerDistanceJoint;
	}
	#endregion
}
