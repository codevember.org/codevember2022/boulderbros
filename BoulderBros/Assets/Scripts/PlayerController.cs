using System;
using System.Collections;
using UnityEngine;
using Rewired;
using Unity.Netcode;
using UnityEngine.Serialization;
using UnityEngine.Events;
using System.Collections.Generic;

public class PlayerController : NetworkBehaviour
{
	[Header("References:")]
	[Header("References/Managers")]
	[SerializeField] private NetworkObject playerNetworkObject;
	[SerializeField] private PlayerAnimator playerAnimator;
	[SerializeField] private PlayerEffectManager playerEffectManager;
	[SerializeField] private PlayerStats playerStats;
	[SerializeField] private PlayerInput playerInput;
	[SerializeField] private PlayerMovement playerMovement;
	[SerializeField] private PlayerRope playerRope;
	[SerializeField] private GroundCheck groundCheck;
	[SerializeField] private WallCheck wallCheck;
	
	[Header("References/Components")]
	[SerializeField] private Rigidbody2D rigidBody2D;
	[SerializeField] private CapsuleCollider2D thisCollider;
	[SerializeField] private Material staminaMat;
	[SerializeField] private PlayerController otherPlayer;
	
	[Header("References/Rewired")]
	public int rewiredPlayerID;
	public Player rewiredPlayer;
	
	[Header("Network:")]
	public int networkPlayerID;

	[Header("States:")]
	[Header("States/grounded")]
	public bool isGrounded;
	public bool isInAir;
	public bool isFalling;
	
	[Header("States/movement")]
	public bool isMoving;
	public bool isMovingX;
	public bool isMovingY;
	public bool isSliding;
	public bool isDragged;
	public bool isExhausted;
	
	[Header("States/run")]
	public bool isRunning;
	public bool wasRunning;
	[SerializeField] private float wasRunningTimer;
	[SerializeField] private float wasRunningCooldown = 0.5f;
	
	[Header("States/jump")]
	public bool jumped;
	public bool ledgeJumped;
	
	[Header("States/climbing wallcheck")]
	public bool currentWallClimbable;
	public bool snappedToWall;
	
	[Header("States/Flipping")]
	public bool facingRight = true;  // For determining which way the player is currently facing.
	public bool isFlipped;
	
	[Header("States/Wallcheck")]
	public bool onWall;
	public bool onOverhang;
	public bool isSideWays;
	public bool isUpsideDown;
	
	[Header("States/climb")]
	public bool isClimbing;
	public bool climbIdle;
	public bool hasStamina = true;
	public float climbingDuration;
	[SerializeField] private float climbingMinDuration = 0.5f;
	
	[Header("States/rope")]
	public bool isOnRope;
	public bool isOnRopeWithOtherPlayer;
	public bool heightWarned;
	
	[Header("Parameters/collider")]
	[SerializeField] private float initialColliderSize = 1.7f;
	[SerializeField] private Vector2 initialColliderOffset;
	[SerializeField] private float climbColliderSize = 0.8f;
	[SerializeField] private Vector2 climbColliderOffset;
	
	

	private void Awake()
	{
		rigidBody2D = GetComponent<Rigidbody2D>();
		thisCollider = GetComponent<CapsuleCollider2D>();
		rewiredPlayer = ReInput.players.GetPlayer(rewiredPlayerID);
	}

    private void Start()
    {
		playerInput.OnPauseGame += GameManager.Instance.PauseGame;
		playerInput.OnClimb += Climb;
		playerInput.OnClimbing += Climbing;
		playerInput.OnStopClimb += StopClimbing;
		playerInput.OnRun += Run;
		playerInput.OnStopRun += StopRun;
    }

    private void Update()
    {
	    //if its online multiplayer and not the owner, dont do shit
	    if(NetworkController.Instance.onlineMultiplayer && !IsOwner) return;
	    
	    if(isClimbing)
		    climbingDuration += Time.deltaTime;
	    else
		    climbingDuration = 0f;
	    
	    //small cooldown after player stopped running so jumpAirspeed can be adjusted
	    if (wasRunning)
	    {
		    wasRunningTimer += Time.deltaTime;
		    if (wasRunningTimer >= wasRunningCooldown)
		    {
			    wasRunningTimer = 0;
			    wasRunning = false;
		    }
	    }
    }

    private void FixedUpdate()
	{
		//if its online multiplayer and not the owner, dont do shit
		if(NetworkController.Instance.onlineMultiplayer && !IsOwner) return;
		
		CanRun();
	}
	
	public void ResetPlayer()
	{
		playerStats.ResetStamina();
		playerAnimator.enabled = true;
		playerAnimator.SetDeathAnimation(false);
		rigidBody2D.bodyType = RigidbodyType2D.Dynamic;
	}

	public void ResetPlayerToStart()
	{
		playerAnimator.FlipSprites();
		playerAnimator.StartPlayerMainMenuAnimation(false);
	}
	
	#region Getters

	public NetworkObject GetPlayerNetworkObject()
	{
		return playerNetworkObject;
	}
	public PlayerInput GetPlayerInput()
	{
		return playerInput;
	}

	public PlayerAnimator GetPlayerAnimator()
	{
		return playerAnimator;
	}

	public PlayerEffectManager GetPlayerEffectmanager()
	{
		return playerEffectManager;
	}
	
	public PlayerMovement GetPlayerMovement()
	{
		return playerMovement;
	}

	public PlayerStats GetPlayerStats()
	{
		return playerStats;
	}
	
	public PlayerRope GetPlayerRope()
	{
		return playerRope;
	}

	public PlayerController GetOtherPlayer()
	{
		return otherPlayer;
	}
	public GroundCheck GetGroundCheck()
	{
		return groundCheck;
	}

	public WallCheck GetWallCheck()
	{
		return wallCheck;
	}

	public Material GetStaminaMaterial()
	{
		return staminaMat;
	}

	public Rigidbody2D GetRigidbody2D()
	{
		return rigidBody2D;
	}

	public CapsuleCollider2D GetCollider2D()
	{
		return thisCollider;
	}
	
	public bool CanLedgeJump()
	{
		return (isClimbing && !ledgeJumped && !jumped && !onOverhang && ClimbedMinDuration());
	}
	
	#endregion
	
	#region RigidBody
	public void SetGravityScale(float value)
	{
		rigidBody2D.gravityScale = value;
	}
	
	#endregion
	
	#region Collider size
	public void AdjustCollider(float size, bool resetToInitialCollider)
	{
		thisCollider.size = new Vector2(thisCollider.size.x, size);
		if (resetToInitialCollider)
		{
			thisCollider.offset = initialColliderOffset;
		}
		else
		{
			thisCollider.offset = climbColliderOffset;
		}
	}

	private IEnumerator DelayedResetCollider()
	{
		yield return new WaitForSeconds(0.2f);
		AdjustCollider(initialColliderSize, true);
	}
	#endregion
	
	#region Running

	private void Run()
	{
		if (!isRunning && isMovingX && hasStamina && !isClimbing && !isInAir && !jumped)
		{
			isRunning = true;
			playerAnimator.SetRunAnimation(isRunning);
		}
	}

	private void CanRun()
	{
		if(isClimbing || !isGrounded || jumped || !isMovingX || !hasStamina)
			StopRun();
	}
	
	public void StopRun()
	{
		if (isRunning)
		{
			isRunning = false;
			wasRunning = true;
			playerAnimator.SetRunAnimation(isRunning);
		}
	}
	
	#endregion
	
	#region Climbing
	
	private bool ClimbedMinDuration()
	{
		return climbingDuration >= climbingMinDuration;
	}
	
	private void Climb()
	{
		if (CanClimb())
		{
			//dont do shit when already climbing
			if (!isClimbing)
			{
				Debug.Log("climb start");
				StopRun();
				isClimbing = true;
				playerAnimator.SetClimbAnimation(true);
				playerAnimator.ChangeSortingOrder(wallCheck.GetClosestWallRenderer());
				//maybe find better place for this:
				groundCheck.SetWallGroundType();
				AdjustCollider(climbColliderSize, false);
			}
		}
	}

	private void Climbing()
	{
		//Debug.Log("Climbing");
		if(!CanClimb() && isClimbing)
			StopClimbing();
		else
			Climb();
	}
	
	private bool CanClimb()
	{
		if (onWall && !jumped && !ledgeJumped && currentWallClimbable && wallCheck.GetFacingWall() && hasStamina && !playerStats.GetStaminaDepleted())
			return true;
		return false;
	}

	private void StopClimbing()
	{
		//AdjustCollider(initialColliderSize, true);
		isClimbing = false;
		snappedToWall = false;
		playerAnimator.SetClimbAnimation(isClimbing);
		playerAnimator.ResetSortingOrder();
		StartCoroutine(DelayedResetCollider());
	}
	#endregion
}