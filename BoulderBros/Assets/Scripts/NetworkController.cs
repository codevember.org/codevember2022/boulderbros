using UnityEngine;
using Unity.Netcode;

public class NetworkController : NetworkBehaviour
{
    public static NetworkController Instance { get; private set; }

    public bool onlineMultiplayer;
    
    [SerializeField] private PlayerController playerController1;
    [SerializeField] private PlayerController playerController2;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    
    void Start()
    {
        NetworkManager.Singleton.OnClientConnectedCallback += HandleClientConnected;
        NetworkManager.Singleton.OnClientDisconnectCallback += HandleClientDisconnected;
    }

    private void HandleClientConnected(ulong clientId)
    {
        onlineMultiplayer = true;
        Debug.Log("Handling Player Connections");
        // Only do this for the client after they connect
        if (NetworkManager.Singleton.IsHost)
        {
            // Host assigns ownership to Player 1
            if (clientId == NetworkManager.Singleton.LocalClientId)
            {
                NetworkObject player1NetworkObject = playerController1.GetComponent<NetworkObject>();
                player1NetworkObject.ChangeOwnership(clientId);  // Host controls Player 1
            }

            // Client assigns ownership to Player 2
            if (clientId != NetworkManager.Singleton.LocalClientId)
            {
                NetworkObject player2NetworkObject = playerController2.GetComponent<NetworkObject>();
                player2NetworkObject.ChangeOwnership(clientId);  // Client controls Player 2
            }
        }
    }
    
    private void HandleClientDisconnected(ulong clientId)
    {
        Debug.Log("Handling Player Disconnections");
        // Only do this for the client after they connect
        var clientIds = NetworkManager.Singleton.ConnectedClientsIds;

        if (clientIds.Count < 1)
        {
            Debug.Log("No clients connected");
            onlineMultiplayer = false;
        }
    }
}
