using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Serialization;

public class PlayerMovement : NetworkBehaviour
{
    [Header("References")] 
    [SerializeField] private PlayerController playerController;
    [SerializeField] private Rigidbody2D playerRigidbody2D;
    [SerializeField] private GroundCheck groundCheck;
    [SerializeField] private WallCheck wallCheck;
    
    [Header("Speeds and Values")]
    [SerializeField] private float horizontalMove;
    [SerializeField] private float verticalMove;
    [Range(0, .3f)] 
    [SerializeField] private float movementSmoothing = .05f;  // How much to smooth out the movement
    [SerializeField] private float moveSpeed = 1;
    [SerializeField] private float walkSpeed = 100;
    [SerializeField] private float runSpeed = 150;
    [SerializeField] private bool wasRunning;
    
    [SerializeField] private float jumpAirSpeed = 100;
    [SerializeField] private float swingForce;
    [SerializeField] private float maxFallSpeed = -50f;
    
    [Header("Transform Speeds and Values")]
    [SerializeField] private Vector3 lastUpdatePos = Vector3.zero;
    [SerializeField] private Vector3 dist;
    [SerializeField] private float transformSpeedX;
    [SerializeField] private float transformSpeedY;
    [SerializeField] private float transformDragThreshold = 0.0025f;
    [SerializeField] private float transformSlideThreshold = 0.2f;

    [Header("Jump Stuff")] 
    [SerializeField] private float jumpStaminaCost;                     //for caching performance save
    [SerializeField] private float jumpForce = 20f;
    [SerializeField] private float wallJumpForce = 20f;                 // Amount of force added when the player jumps.
    [SerializeField] private float hasJumpedTimer;
    [SerializeField] private float hasJumpedCooldown = 0.5f;
    [SerializeField] private bool jumpReleased;
    [SerializeField] private float lastJumpTime;
    [SerializeField] private float gravityScaleFasterFall = 1.75f;
    [Space(5)]
    [SerializeField] private Vector2 ledgeJumpForce;
    [SerializeField] private float ledgeJumpTimer;
    [SerializeField] private float ledgeJumpCooldown = 0.15f;
    [SerializeField] private float ledgeJumpDelay = 0.15f;
    
    [Header("Climbing")]
    [SerializeField] private float baseClimbSpeed = 10;
    [Tooltip("additional multiplication factor that reduces speed if player is on Overhang")]
    [SerializeField] private float overhangClimbSpeedFactor = 0.8f;
    [SerializeField] private float currentClimbSpeed;
    [Tooltip("defines the speed multiplication for base speed depending on wall angle lowest = 45 - 60?, highest 175?+")]
    [SerializeField] private float[] climbingSpeedFactors = new float[10];
    
    [Header("Flipping")]
    private Vector3 velocity = Vector3.zero;

    private void Start()
    {
        //Caching for performance
        groundCheck = playerController.GetGroundCheck();
        wallCheck = playerController.GetWallCheck();
        jumpStaminaCost = playerController.GetPlayerStats().GetJumpStaminaCosts();
        
        //Events
        PlayerInput playerInput = playerController.GetPlayerInput();
        playerInput.OnJump += Jump;
        playerInput.OnJumpButtonUp += JumpHeightCut;
        playerInput.OnJumpButtonUp += JumpReleased;
        
        //if not mainMenu Animations running flip Players like normally
         if(!MenuManager.Instance.GetMainMenuEnabled() && !gameObject.GetComponent<PlayerController>().isFlipped)
             Flip();
    }

    private void Update()
    {
        //if its online multiplayer and not the owner, dont do shit
        if(NetworkController.Instance.onlineMultiplayer && !IsOwner) return;
        
        //GetInputs
        horizontalMove = playerController.GetPlayerInput().GetMovementInput().x * moveSpeed;
        verticalMove = playerController.GetPlayerInput().GetMovementInput().y * moveSpeed;
        
        //Cooldowns
        if (playerController.ledgeJumped)
        {
            ledgeJumpTimer += Time.deltaTime;
            if (ledgeJumpTimer >= ledgeJumpCooldown)
            {
                ledgeJumpTimer = 0;
                playerController.ledgeJumped = false;
                playerController.GetPlayerAnimator().SetLedgeJumpAnimation(false);
            }
        }

        //Jump Cooldown for can Climb and air control etc.
        if (playerController.jumped)
        {
            hasJumpedTimer += Time.deltaTime;
            
            if (hasJumpedTimer > hasJumpedCooldown || jumpReleased)
            {
                playerController.jumped = false;
                hasJumpedTimer = 0;
            }
        }
        
        //lastJumpTime for calculating Jump Height Cut and Stamina Cost
        if(playerController.jumped && !jumpReleased)
            lastJumpTime += Time.deltaTime;
    }

    private void FixedUpdate()
    {
        //if its online multiplayer and not the owner, dont do shit
        if(NetworkController.Instance.onlineMultiplayer && !IsOwner) return;
        
        //UpdateMovement
        Move(horizontalMove * Time.deltaTime, verticalMove * Time.deltaTime);
        IsMoving();
        IsFalling();
        //HasJumped();
        //TransformBeingDragged();
        IsSliding();
        BeingDragged();
    }

    #region Movement
    public void Move(float moveX, float moveY)
    {
        PlayerStats playerStats = playerController.GetPlayerStats();
        SetMovementSpeed();
        SetPlayerGravityState();
        playerStats.ControlStaminaLoss();

        //if (playerController.isGrounded || playerController.isInAir) use this if air controls should be enabled
        //only control the player if grounded or recently jumped
        if (playerController.isGrounded || playerController.jumped)
        {
            // Move the character by finding the target velocity
            Vector3 targetVelocity = new Vector2(moveX, playerRigidbody2D.linearVelocity.y);
            
            // And then smoothing it out and applying it to the character
            playerRigidbody2D.linearVelocity = Vector3.SmoothDamp(playerRigidbody2D.linearVelocity, targetVelocity, ref velocity, movementSmoothing);	
        }
        else
        {
            if(playerController.isOnRope)
                SwingRope();
        }
        //check if player can climb, if so get x and y input to control character 
        if (playerController.isClimbing && playerController.hasStamina && playerController.currentWallClimbable && !playerController.jumped)
        {
            playerRigidbody2D.linearVelocity = transform.up * NormalizeClimbInput(moveX,moveY);
        }
        
        //If snapped to wall and idle climbing set rigidbody constraints 
        if((verticalMove == 0 && horizontalMove == 0) && playerController.isClimbing && playerController.snappedToWall && !playerController.jumped)
        {
            playerRigidbody2D.constraints = RigidbodyConstraints2D.FreezePosition | RigidbodyConstraints2D.FreezeRotation;
            playerController.climbIdle = true;
        }
        else
        {
            playerRigidbody2D.constraints = ~RigidbodyConstraints2D.FreezePosition | RigidbodyConstraints2D.FreezeRotation;
            playerController.climbIdle = false;
        }

        if (!playerController.isClimbing)
        {
            // If the input is moving the player right and the player is facing left...
            if (moveX > 0 && !playerController.facingRight)
            {
                // ... flip the player.
                Flip();
            }
            // Otherwise if the input is moving the player left and the player is facing right...
            else if (moveX < 0 && playerController.facingRight)
            {
                // ... flip the player.
                Flip();
            }
        }
    }

    public void FreezeMovement()
    {
        playerRigidbody2D.constraints = RigidbodyConstraints2D.FreezePosition | RigidbodyConstraints2D.FreezeRotation;
        playerRigidbody2D.linearVelocity = Vector2.zero;
    }
    public void ResetMovement()
    {
        playerRigidbody2D.constraints = ~RigidbodyConstraints2D.FreezePosition | RigidbodyConstraints2D.FreezeRotation;
    }
    
    private void SetMovementSpeed()
    {
        if (playerController.isGrounded)
        {
            if (playerController.hasStamina && playerController.isClimbing)
            {
                CalculateClimbSpeed();
                moveSpeed = currentClimbSpeed;
            }
            else
            {
                if (playerController.isRunning)
                {
                    moveSpeed = runSpeed;
                }
                else
                {
                    moveSpeed = walkSpeed;
                }
            }	
        }
        else
        {
            if (playerController.hasStamina && playerController.isClimbing)
            {
                CalculateClimbSpeed();
                moveSpeed = currentClimbSpeed;
            }
            else
            {
                if (playerController.jumped)
                {
                    CalculateJumpAirSpeed();
                    moveSpeed = jumpAirSpeed;
                }
            }
        }
    }

    public void Flip()
    {
        // Switch the way the player is labelled as facing.
        playerController.facingRight = !playerController.facingRight;
	
        // Multiply the player's x local scale by -1.
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
        playerController.isFlipped = !playerController.isFlipped;
	
        UIManager.Instance.FlipStaminaCircle(playerController);
        UIManager.Instance.FlipSpeechBubble(playerController);
        playerController.GetPlayerEffectmanager().FlipDust();
    }
    
    private void CalculateJumpAirSpeed()
    {
        if (playerController.isRunning || playerController.wasRunning)
            jumpAirSpeed = runSpeed;
        else
            jumpAirSpeed = walkSpeed;
    }
    
    private void SetPlayerGravityState()
    {
        if (playerController.isGrounded && !playerController.isClimbing && !Mathf.Approximately(playerRigidbody2D.gravityScale, 1))
        {
            playerController.SetGravityScale(1);
        }
        else
        {
            if (playerController.isClimbing && !Mathf.Approximately(playerRigidbody2D.gravityScale, 0))
            {
                playerController.SetGravityScale(0);
            }
            else
            {
                if (playerController.isInAir)
                {
                    if (playerController.isFalling && (!playerController.isOnRope && !playerController.isOnRopeWithOtherPlayer) && !Mathf.Approximately(playerRigidbody2D.gravityScale, gravityScaleFasterFall))
                    {
                        playerController.SetGravityScale(gravityScaleFasterFall);
                    }
                    else
                    {
                        if (!playerController.isFalling && !Mathf.Approximately(playerRigidbody2D.gravityScale, 1))
                        {
                            playerController.SetGravityScale(1);
                        }
                    }
                }
            }
        }
            
    }

    private void IsMoving()
    {
        bool isMovingHorizontally = horizontalMove != 0;
        bool isMovingVertically = verticalMove != 0;

        playerController.isMoving = isMovingHorizontally || isMovingVertically;
        playerController.isMovingX = isMovingHorizontally;
        playerController.isMovingY = isMovingVertically;
    }
    
    private void IsFalling()
    {
        if (playerController.isFalling)
        {
            playerRigidbody2D.linearVelocity = new Vector2(playerRigidbody2D.linearVelocity.x,
                Mathf.Max(playerRigidbody2D.linearVelocity.y, maxFallSpeed));
        }
    }
    private float NormalizeClimbInput(float moveX, float moveY)
    {
        var climbMove = 0f;
        if (Mathf.Abs(moveX) >= Mathf.Abs(moveY))
        {
            climbMove =  moveX;
            if ((playerController.facingRight && playerController.onOverhang) || (!playerController.facingRight && !playerController.onOverhang))
                climbMove *= -1f;
        }

        else
        {
            climbMove =  moveY;
            if(playerController.isUpsideDown)
                climbMove *= -1f;
        }
        return climbMove;
    }
    
    public Vector2 GetRigidbodyVelocity()
    {
		return playerRigidbody2D.linearVelocity;
    }

	public Vector2 GetTransformSpeeds()
    {
		Vector2 transformSpeeds = new Vector2(transformSpeedX, transformSpeedY);
		return transformSpeeds;
    }

	private void IsSliding()
    {
		if ((!playerController.isGrounded && !playerController.isClimbing && playerController.onWall) && (transformSpeedY <= -transformSlideThreshold || transformSpeedY >= transformSlideThreshold) && wallCheck.GetCurrentWallAngle() <= 87.5)
		{
            playerController.isSliding = true;
		}
        else
        {
            playerController.isSliding = false;
        }
    }

	private void BeingDragged()
    {
        //get current TransformSpeed
        transformSpeedX = playerRigidbody2D.linearVelocity.x;
        transformSpeedY = playerRigidbody2D.linearVelocity.y;
        
        if(!playerController.facingRight)
            transformSpeedX *= -1;
        
		if (playerController.isGrounded && playerController.isOnRopeWithOtherPlayer && !playerController.isMovingX && (transformSpeedX <= -transformDragThreshold || transformSpeedX >= transformDragThreshold))
		{
            playerController.isDragged = true;
		}
		else
            playerController.isDragged = false;
        
        playerController.GetPlayerAnimator().UpdateTransformSpeeds(transformSpeedX,transformSpeedY);
		
	}

	public bool GetFallOrSlideOrDrag()
    {
		if ((playerController.isSliding || playerController.isDragged || (!playerController.isClimbing && !playerController.isGrounded) && playerController.GetPlayerStats().GetCurrentStamina() >= 75))
		{
			return true;
		}
		
        return false;
    }
    
    private void SwingRope()
    {
        playerRigidbody2D.AddForce(new Vector2(1, 0) * (horizontalMove * swingForce));
    }
    
    #endregion
    
    #region Jump
    private void Jump()
    {
        PlayerStats playerStats = playerController.GetPlayerStats();
        if (playerStats.GetCurrentStamina() >= jumpStaminaCost)
        {
            if (playerController.isGrounded && !playerController.isClimbing)
            {
                //Debug.Log("Should be normal Jumping");
                InitJump();
                AddJumpForce(jumpForce);
                //playerStats.StaminaBurnOnce(jumpStaminaCost);
                StartCoroutine(DelayedStaminaBurn());
                return;
            }
            if (playerController.isClimbing)
            {
                //Debug.Log("Should wall Jump");
                InitJump();
                AddJumpForce(wallJumpForce);
                //playerStats.StaminaBurnOnce(jumpStaminaCost);
                StartCoroutine(DelayedStaminaBurn());
            }
        }
    }

    private IEnumerator DelayedStaminaBurn()
    {
        yield return new WaitForSeconds(0.5f);
        playerController.GetPlayerStats().StaminaBurnJump(jumpStaminaCost, lastJumpTime);
        //Debug.Log("should calc: " + jumpStaminaCost + " and " + lastJumpTime);
    }
    
    private void InitJump()
    {
        ResetLastJump();
        playerController.jumped = true;
        playerController.GetPlayerAnimator().JumpAnimation(playerController.isGrounded,playerController.jumped);
    }

    private void AddJumpForce(float value)
    {
        playerController.GetRigidbody2D().AddForce(new Vector2(0f, value), ForceMode2D.Impulse);
    }

    private void AddJumpForce(Vector2 force)
    {
        playerController.GetRigidbody2D().AddForce(force, ForceMode2D.Impulse);
    }
    
    private void JumpHeightCut()
    {
        //only cut jump height if is before maximum jump Time
        if (playerController.jumped)
        {
            //Debug.Log("JumpHeightCut");
            Vector2 currentVelocity = playerRigidbody2D.linearVelocity;
            playerRigidbody2D.linearVelocity = new Vector2(currentVelocity.x, currentVelocity.y* 0.5f);
        }
    }
    
    private void JumpReleased()
    {
        playerController.jumped = false;
        jumpReleased = true;
        
    }

    private void ResetLastJump()
    {
        lastJumpTime = 0f;
        jumpReleased = false;
    }
    
    public void LedgeJump()
    {
        Vector2 ledgeJumpVector = new Vector2(ledgeJumpForce.x, ledgeJumpForce.y);
        if(!playerController.facingRight)
            ledgeJumpVector.x *= -1;
        
        playerController.GetPlayerAnimator().SetLedgeJumpAnimation(true);
        InitJump();
        AddJumpForce(ledgeJumpForce.y);
        StartCoroutine(LedgeJumpDelayForce(ledgeJumpVector));
        Debug.Log("LedgeJumped with Vector of: " + ledgeJumpVector);
    }

    private IEnumerator LedgeJumpDelayForce(Vector2 force)
    {
        yield return new WaitForSeconds(ledgeJumpDelay);
        AddJumpForce(force);
    }

    //if player just jumped air control also activated until grounded again
    // private void HasJumped()
    // {
    //     if (playerController.jumped)
    //         hasJumpedTimer += Time.deltaTime;
    //
    //     if (hasJumpedTimer >= hasJumpedCooldown)
    //     {
    //         playerController.jumped = false;
    //         hasJumpedTimer = 0;
    //         if (playerController.isGrounded)
    //         {
    //             
    //         }
    //     }
    // }

    #endregion
    
    #region Climbing
    private void CalculateClimbSpeed()
    {
        float targetClimbSpeedFactor = 0;
        float currentWallAngle = wallCheck.GetCurrentWallAngle();
        
        if (currentWallAngle >= 45)
            targetClimbSpeedFactor = climbingSpeedFactors[0];
        if (currentWallAngle >= 60)
            targetClimbSpeedFactor = climbingSpeedFactors[1];
        if (currentWallAngle >= 75)
            targetClimbSpeedFactor = climbingSpeedFactors[2];
        if (currentWallAngle >= 90)
            targetClimbSpeedFactor = climbingSpeedFactors[3];
        if (currentWallAngle >= 100)
            targetClimbSpeedFactor = climbingSpeedFactors[4];
        if (currentWallAngle >= 120)
            targetClimbSpeedFactor = climbingSpeedFactors[5];
        if (currentWallAngle >= 135)
            targetClimbSpeedFactor = climbingSpeedFactors[6];
        if (currentWallAngle >= 150)
            targetClimbSpeedFactor = climbingSpeedFactors[7];
        if (currentWallAngle >= 160)
            targetClimbSpeedFactor = climbingSpeedFactors[8];
        if (currentWallAngle >= 175)
            targetClimbSpeedFactor = climbingSpeedFactors[9];
        if(playerController.isUpsideDown)
            targetClimbSpeedFactor = climbingSpeedFactors[10];

        if (!playerController.onOverhang || (playerController.isUpsideDown && playerController.onOverhang))
        {
            currentClimbSpeed = baseClimbSpeed * targetClimbSpeedFactor;
        }
        else
        {
            currentClimbSpeed = baseClimbSpeed * targetClimbSpeedFactor * overhangClimbSpeedFactor;
        }

        //Debug.Log("Speed was calculated");
    }
    
    private void SetFriction(RaycastHit2D hitDown)
    {
        //RaycastHit2D hitDown = Physics2D.Raycast(transform.position, transform.up * -1, groundCheckDistance, wallLayerMask);
    
        if (playerController.isClimbing)
            playerRigidbody2D.sharedMaterial.friction = 1;
        else
        {
            if (hitDown)
            {
                Vector2 groundNormal = playerController.GetGroundCheck().GetGroundNormal();
                groundNormal = hitDown.normal;
                playerRigidbody2D.sharedMaterial.friction = 1f - (groundNormal.x * -1f);
                playerRigidbody2D.sharedMaterial.friction = Mathf.Clamp(playerRigidbody2D.sharedMaterial.friction, 0f, 1f);
            }
            else
                playerRigidbody2D.sharedMaterial.friction = 0f;
        }
    }
    
    #endregion
}
