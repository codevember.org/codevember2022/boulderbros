using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class LoopMovement : MonoBehaviour
{
    //[SerializeField] private bool visible;
    [SerializeField] private bool play;
    [SerializeField] private bool loop = false;
    [SerializeField] private float loopTime;
    [SerializeField] private Vector3 startPosition;
    [SerializeField] private Vector3 targetPosition;
    [SerializeField] private float timeElapsed;
    [Range(0, 100)]
    [SerializeField] private float offset;
    // Start is called before the first frame update
    void Start()
    {
        startPosition = transform.position;
        timeElapsed = loopTime * offset / 100f;   
    }

    //private void OnBecameVisible()
    //{
    //    visible = true;
    //}

    //private void OnBecameInvisible()
    //{
    //    visible = false;
    //}

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Application.isPlaying)
        {
            if (play)
                Lerp();
        }
    }

    private void OnValidate()
    {
        if(!Application.isPlaying)
            startPosition = transform.position;
    }

    private void Lerp()
    {
        if (timeElapsed < loopTime)
        {
            transform.position = Vector3.Lerp(startPosition, targetPosition, timeElapsed / loopTime);
            timeElapsed += Time.deltaTime;
        }
        else
        {
            transform.position = targetPosition;
            if (loop) { }
                //Reset();
            else
            {
                gameObject.SetActive(false);
                this.enabled = false;
            }       
        }
    }

    private void Reset()
    {
        if (Application.isPlaying)
        {
            transform.position = startPosition;
            timeElapsed = 0;
        }  
    }

    public float GetLooptime()
    {
        return loopTime;
    }

    public float GetTargetPosX()
    {
        return targetPosition.x;
    }
}
