﻿using UnityEngine.Audio;
using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Random = UnityEngine.Random;

public class AudioManager : MonoBehaviour
{
    public bool audioMuted;
    public bool playSounds;
    public bool playMusic;
#pragma warning disable
    [SerializeField]private bool soundsStopped = false;
    [SerializeField]private bool musicStopped = false;
#pragma warning restore
    public Sound[] sounds;

    public static AudioManager Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
            
        else
        {
            Destroy(gameObject);
            return;
        }

        foreach(Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;

            s.source.volume = s.volume;
            s.source.pitch = s.pitch;

            s.source.loop = s.loop;
        }
    }

    //private void LateUpdate()
    //{
    //    if(sounds)
    //}

    public void Play(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found!");
            return;
        }
        if (!audioMuted)
        {
            if((s.type.Equals("Sound") && playSounds) ||(s.type.Equals("Music") && playMusic))
            {
                if (s.fadeIn)
                {
                    Debug.Log("Fading in: " + name + " Sound now");
                    Fade(s.name, s.fadeDuration, s.volume, true);
                }
                Debug.Log("Playing: " + name + " Sound now");
                s.source.Play();         
            }
            
        }      
    }

    public void Stop(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found!");
            return;
        }
        s.source.Stop();
    }

    public void StopSounds()
    {
        for (int i = 0; i < sounds.Length; i++)
        {
            if (sounds[i].type.Equals("Sound"))
                Stop(sounds[i].name);
        }
        Debug.Log("Sounds Stopped");
        soundsStopped = true;
    }

    public void StopMusic()
    {
        for (int i = 0; i < sounds.Length; i++)
        {
            if (sounds[i].type.Equals("Music"))
                Stop(sounds[i].name);
        }
        Debug.Log("Music Stopped");
        musicStopped = true;
    }

    public void Pause(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found!");
            return;
        }
        //s.volume = 0;
        s.source.Pause();
    }

    public void UnPause(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found!");
            return;
        }
        //s.volume = 0;
        s.source.UnPause();
    }

    public void PauseSounds()
    {
        for (int i = 0; i < sounds.Length; i++)
        {
            if (sounds[i].type.Equals("Sound"))
                sounds[i].source.Pause();
        }
        Debug.Log("Sounds paused");
    }

    public void UnPauseSounds()
    {
        for (int i = 0; i < sounds.Length; i++)
        {
            if (sounds[i].type.Equals("Sound"))
                sounds[i].source.UnPause();
        }
        Debug.Log("Sounds unpaused");
    }

    public void PlayButtonSound()
    {
        if (!audioMuted)
        {
            Debug.Log("Button sound should be played now");
            Play(sounds[0].name);
        }
    }

    public void Fade(string name, float duration, float targetVolume, bool fadeIn)
    {
        StartCoroutine(StartFade(name, duration, targetVolume, fadeIn));
    }


    public IEnumerator StartFade(string name, float duration, float targetVolume, bool fadeIn)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found!");
            yield return new WaitForEndOfFrame();
        }

        AudioSource audioSource = s.source;
        float currentTime = 0;
        float start;
        if (fadeIn)
            start = 0;
        else
            start = audioSource.volume; 

        while (currentTime < duration)
        {
            currentTime += Time.deltaTime;
            audioSource.volume = Mathf.Lerp(start, targetVolume, currentTime / duration);
            yield return null;
        }
        if (targetVolume == 0)
            Stop(name);
        yield break;
    }
}
