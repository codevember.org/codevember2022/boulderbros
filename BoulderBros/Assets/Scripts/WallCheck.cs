// using System;
// using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using Unity.Netcode;

public class WallCheck : NetworkBehaviour
{
	[Header("References/Managers")] 
	[SerializeField] private PlayerController playerController;
	[SerializeField] private PlayerAnimator playerAnimator;
	[SerializeField] private CapsuleCollider2D playerCollider;
	
	[Header("Wallcheck/Forces and Values")]
	[SerializeField] private LayerMask wallLayerMask;
	[SerializeField] private float wallMaxDistance;
	[SerializeField] private Vector3 wallCheckOffset;
	
	[Header("Wallcheck/bools")]
	[SerializeField] private bool facingWall;
	[SerializeField] private bool onLedgeLeft;
	[SerializeField] private bool onLedgeRight;
	[SerializeField] private bool wallRight;
	[SerializeField] private bool wallUpRight;
	[SerializeField] private bool wallDownRight;
	[SerializeField] private bool wallLeft;
	[SerializeField] private bool wallUpLeft;
	[SerializeField] private bool wallDownLeft;
	[SerializeField] private bool wallUp;

	[Header("Wallcheck/raycast check")] 
	[SerializeField] private Vector2 wallCheckCenter;
	[SerializeField] private Collider2D[] circleOverlapColliders;
	[SerializeField] private GameObject closestWall;
	[SerializeField] private Collider2D closestWallCollider;
	[SerializeField] private SpriteRenderer closestWallRenderer;
	[SerializeField] private Vector2 wallNormal;
	[SerializeField] private Vector2 closestColPoint;
	private Vector2 playerRotationNormal;
	[SerializeField] private float minimumClimbAngle = 45f;
	[SerializeField] private float sideWaysThreshold = 84.9f;
	[SerializeField] private float upsideDownThreshold = 92.5f;
	[SerializeField] private float upsideDownMaximumThreshold = 112.5f;
	[SerializeField] private float currentWallAngle;

	[Header("Wallcheck/ledge Detection")] 
	[SerializeField] private Vector2 ledgeCheckOffset;
	
	[Header("Wallcheck/Player rotation adjustment")]
	[SerializeField] private bool rotationAdjusted;
	[SerializeField] private float rotationTimer;
	[SerializeField] private float rotationAdjustmentCooldown;
	[SerializeField] private float rotatePlayerLerpFactor = 0.25f;

	[Header("Wallcheck/Player WallSnapping")]
	[SerializeField] private bool beingSnapped;
	[SerializeField] private float wallSnapForce;

	private void Start()
	{
		playerAnimator = playerController.GetComponent<PlayerAnimator>();
	}

	private void Update()
	{
		//if its online multiplayer and not the owner, dont do shit
		if(NetworkController.Instance.onlineMultiplayer && !IsOwner) return;
		
		//updating the center for the overlapCircle and Raycasts
		wallCheckCenter = playerController.GetCollider2D().bounds.center;
		
		//snapping and rotation adjustments
		if (beingSnapped && closestWallCollider != null)
			IsSnappedToWall();
		
		if (rotationAdjusted)
		{
			rotationTimer += Time.deltaTime;
			if (rotationTimer >= rotationAdjustmentCooldown)
			{
				rotationTimer = 0;
				rotationAdjusted = false;
			}
		}
	}

	private void FixedUpdate()
	{
		//if its online multiplayer and not the owner, dont do shit
		if(NetworkController.Instance.onlineMultiplayer && !IsOwner) return;
		
		GetClosestWall();
		RotatePlayerToWall(playerController.isClimbing);
		if(playerController.CanLedgeJump())
			CheckForLedge();
		SnapToWall();
	}

	private void OnDrawGizmos()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere(wallCheckCenter, wallMaxDistance);
	}
	
	#region Closest Wall detection
	private void GetClosestWall()
	{
		circleOverlapColliders = Physics2D.OverlapCircleAll(wallCheckCenter, wallMaxDistance, wallLayerMask);

		if (circleOverlapColliders.Length > 0)
		{
			playerController.onWall = true;
			
			foreach (Collider2D col in circleOverlapColliders)
			{
				if (closestWall == null)
				{
					//Debug.Log("was null: setting ClosestWall and normal");
					UpdateClosestWall(col,wallCheckCenter);
				}

				if (Vector2.Distance(col.ClosestPoint(wallCheckCenter), wallCheckCenter) < Vector2.Distance(closestWallCollider.ClosestPoint(wallCheckCenter), wallCheckCenter))
				{
					//Debug.Log(col + " was closer than actual closestPoint");
					if (col == closestWallCollider)
					{
						//Debug.Log(col + " is the same as "+ closestWallCollider);
						SetWallNormal(closestWallCollider);
						SetWallDirection(closestWallCollider.ClosestPoint(wallCheckCenter));
					}
					else
					{
						//Debug.Log(col + " is not the same as "+ closestWallCollider);
						UpdateClosestWall(col, wallCheckCenter);
					}
				}
				else
				{
					SetWallNormal(closestWallCollider);
					SetWallDirection(closestWallCollider.ClosestPoint(wallCheckCenter));
				}
				// if(col.gameObject != closestWall)
				// 	ResetWallSnapCooldown();
			}
			CheckWallAngleClimbable();
			CheckOverhang();
			CheckPlayerOrientation();
			FacingWall();
		}
		else
		{
			playerController.onWall = false;
			ResetClosestWall();
		}
		//Debug.Log("closestWall: " + closestWall);
	}

	private void SetClosestWall(Collider2D col)
	{
		closestWall = col.gameObject;
		closestWallCollider = col;
		closestWallRenderer = col.GetComponent<SpriteRenderer>();
	}
	
	private void UpdateClosestWall(Collider2D col, Vector2 circleCenter)
	{
		SetClosestWall(col);
		SetWallNormal(col);
		SetWallDirection(col.ClosestPoint(circleCenter));
	}
	
	private void ResetClosestWall()
	{
		closestWall = null;
		closestWallRenderer = null;
		closestWallCollider = null;
		wallNormal = Vector2.zero;
		currentWallAngle = 0;
		wallRight = wallLeft = false;
		playerController.onOverhang = false;
		ResetFacingWall();
	}

	public GameObject GetClosestWallObject()
	{
		return closestWall;
	}
	
	public SpriteRenderer GetClosestWallRenderer()
	{
		return closestWallRenderer;
	}
	
	
	#endregion
	
	#region Wallfacing and walldirection
	private void FacingWall()
	{
		if (beingSnapped)
		{
			facingWall = true;
			return;
		}
		// if player upside down wallRight is probably closer even though player is facing left and vice versa
		if ((wallLeft && !playerController.facingRight) || (wallRight && playerController.facingRight) || 
		    (playerController.isSideWays && ((wallRight && !playerController.facingRight) || (wallLeft && playerController.facingRight))))
		{
			facingWall = true;
			playerAnimator.SetFacingWallAnimation(facingWall);
			return;
		}
		ResetFacingWall();
	}

	private void ResetFacingWall()
	{
		facingWall = false;
		playerAnimator.SetFacingWallAnimation(facingWall);
	}
	
	public bool GetFacingWall()
	{
		return facingWall;
	}
	
	private void SetWallDirection(Vector2 closestPos)
	{
		if (closestPos.x > wallCheckCenter.x)
		{
			wallRight = true;
			wallLeft = false;
		}
		else
		{
			wallLeft = true;
			wallRight = false;
		}
	}
	#endregion
	
	#region Wallangle and normals
	
	//calculates the angle of the player like its shown in the inspector (-180 to 180) for easier understandability
	private float InspectorAngle()
	{
		//calculated after facing wall etc. so too late
		float playerRotation = transform.localRotation.eulerAngles.z;
		playerRotation %= 360;
		playerRotation = playerRotation>180?playerRotation-360 : playerRotation;
		return playerRotation;
	}
	
	private void SetWallNormal(Collider2D col)
	{
		Vector2 oldWallNormal = wallNormal;
		Vector2 closestPoint = col.ClosestPoint(wallCheckCenter);
		wallNormal = (wallCheckCenter - closestPoint).normalized;

		if (WallNormalChanged(oldWallNormal, wallNormal))
		{
			ResetSnappedToWall();
		}
		
		playerRotationNormal = wallNormal;
		currentWallAngle = Vector3.Angle(wallNormal, Vector3.up);
	}

	private bool WallNormalChanged(Vector2 oldNormal, Vector2 newNormal)
	{
		float normalDifference = Mathf.Abs(oldNormal.x - newNormal.x);
		if (normalDifference > 0.05f)
			return true;
		return false;
	}
	private void CheckWallAngleClimbable()
	{
		if (playerController.isFlipped)
			playerRotationNormal *= -1;
 
		if(currentWallAngle >= minimumClimbAngle)
			playerController.currentWallClimbable = true;
		else
		{
			playerController.currentWallClimbable = false;
		}
	}
	
	public float GetCurrentWallAngle()
	{
		return currentWallAngle;
	}
	
	private void RotatePlayerToWall(bool climb)
	{
		if (climb && playerController.onWall && !rotationAdjusted)
		{
			var rotateFactorPlayer = Quaternion.FromToRotation(transform.right, playerRotationNormal);
			transform.rotation = Quaternion.Lerp(transform.rotation, (rotateFactorPlayer * transform.rotation), rotatePlayerLerpFactor);
			rotationAdjusted = true;
		}
		else if (!playerController.isClimbing && !beingSnapped && !(transform.rotation == Quaternion.identity))
		{
			//transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.identity, rotatePlayerLerpFactor);
			transform.rotation = Quaternion.identity;
			//Debug.Log("Resetting player rotation");
		}
	}
	#endregion
	
	#region Checks for states: Overhang, Sideways/Upsidedown and onLedge
	private void CheckOverhang()
	{
		if (wallNormal.y < -0.1f)
			playerController.onOverhang = true;
		else
			playerController.onOverhang = false;
	}

	private void CheckPlayerOrientation()
	{
		var currentAngle = InspectorAngle();
		//Debug.Log("currentAngle: " + currentAngle);
		if ((currentAngle > sideWaysThreshold && currentAngle < upsideDownMaximumThreshold) || (currentAngle < -sideWaysThreshold && currentAngle > -upsideDownMaximumThreshold))
		{
			playerController.isSideWays = true;
			if (currentAngle > upsideDownThreshold || currentAngle < -upsideDownThreshold)
				playerController.isUpsideDown = true;
			else
				playerController.isUpsideDown = false;
		}
		else
		{
			playerController.isSideWays = false;
			playerController.isUpsideDown = false;
		}
	}

	private void CheckForLedge()
	{
		Vector3 upperDirection;
		Vector3 lowerDirection;
		Vector3 downVector;
		downVector = transform.up * -1f;
		if (playerController.facingRight)
		{
			upperDirection = transform.right;
		}
		else
		{
			upperDirection = transform.right * -1f;
		}
		lowerDirection = (upperDirection + downVector).normalized;
		Vector2 origin = wallCheckCenter + ledgeCheckOffset;
		
		RaycastHit2D hitLedgeForward= Physics2D.Raycast(origin, upperDirection, wallMaxDistance * 0.75f, wallLayerMask);
		RaycastHit2D hitLedgeLow = Physics2D.Raycast(origin, lowerDirection, wallMaxDistance, wallLayerMask);
		
		Debug.DrawRay(origin, upperDirection * (wallMaxDistance * 0.75f), Color.white);
		Debug.DrawRay(origin, lowerDirection * wallMaxDistance, Color.white);
 
		//&& playerController.canLedgeJump
		if (!hitLedgeForward && hitLedgeLow)
		{
			playerController.ledgeJumped = true;
			playerController.GetPlayerAnimator().SetLedgeJumpAnimation(true);
			playerController.GetPlayerMovement().LedgeJump();
		}
	}
	#endregion

	#region WallSnapping
	private void SnapToWall()
	{
		if (playerController.isClimbing && !playerController.snappedToWall && !playerController.jumped)
		{
			Debug.Log(playerController+ " Snapping to wall: " + closestWall);
			beingSnapped = true;
			if (playerController.facingRight) 
			{ 
				playerController.GetRigidbody2D().AddForce(transform.right * wallSnapForce, ForceMode2D.Impulse);
			}
			if (!playerController.facingRight)
			{
				playerController.GetRigidbody2D().AddForce(-transform.right * wallSnapForce, ForceMode2D.Impulse);
			}
			//Debug.Log("Snapped to Wall");
		}
	}

	private void IsSnappedToWall()
	{
		if (playerCollider.IsTouching(closestWallCollider))
		{
			playerController.snappedToWall = true;
			beingSnapped = false;
		}
	}

	private void ResetSnappedToWall()
	{
		playerController.snappedToWall = false;
	}
	#endregion
}
