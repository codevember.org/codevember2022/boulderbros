using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

public class Cheat : MonoBehaviour
{
    public float force;
    public Rigidbody2D thisRigid;
    [SerializeField] private int rewiredPlayerID;
    [SerializeField] private Player rewiredPlayer;


    private void Start()
    {
        thisRigid = gameObject.GetComponent<Rigidbody2D>();
        rewiredPlayer = ReInput.players.GetPlayer(rewiredPlayerID);
    }
    private void Update()
    {
        if(thisRigid != null)
        {
            if (rewiredPlayer.GetButton("flyLeft"))
            {
                thisRigid.AddForce(Vector2.left * force);
            }
            if (rewiredPlayer.GetButton("flyRight"))
            {
                thisRigid.AddForce(Vector2.right * force);
            }
            if (rewiredPlayer.GetButton("flyUp"))
            {
                thisRigid.AddForce(Vector2.up * force);
            }
        }
        
    }
}
