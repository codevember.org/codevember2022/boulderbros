using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TutorialTooltip : MonoBehaviour
{
    public string lastActiveInputDevice;
    public float tooltipDuration;
    public float fadeDuration;
    public bool keyboardActive = false;
    public bool xboxGamepadActive = false;
    public bool psGamepadActive = false;

    //Input Types: Keyboard, Sony DualSense, Xbox One Controller
    // Start is called before the first frame update
    void Start()
    {
        CheckInputDevice();
    }

    public void CheckInputDevice()
    {
        lastActiveInputDevice = GameManager.Instance.GetInputController().GetInputDeviceNamePlayer1();

        if (lastActiveInputDevice.Contains("Keyboard"))
        {
            keyboardActive = true;
            xboxGamepadActive = false;
            psGamepadActive = false;
        }
        if (lastActiveInputDevice.Contains("Xbox"))
        {
            keyboardActive = false;
            xboxGamepadActive = true;
            psGamepadActive = false;
        }
        if (lastActiveInputDevice.Contains("Sony"))
        {
            keyboardActive = false;
            xboxGamepadActive = false;
            psGamepadActive = true;
        }
    }
    public void Fade(TextMeshProUGUI text, float duration, float newAlpha)
    {
        StartCoroutine(FadeOut(text, duration, newAlpha));
    }
    public void Fade(SpriteRenderer render, float duration, float newAlpha)
    {
        StartCoroutine(FadeOut(render, duration, newAlpha));
    }

    IEnumerator FadeOut(TextMeshProUGUI text, float duration, float newAlpha)
    {
        yield return new WaitForSeconds(tooltipDuration);
        float originalAlpha = text.color.a;
        float elapsed = 0f;

        while (elapsed < duration)
        {
            elapsed += Time.deltaTime;
            float alpha = Mathf.Lerp(originalAlpha, newAlpha, elapsed / duration);
            text.color = new Color(text.color.r, text.color.g, text.color.b, alpha);
            yield return null;
        }
        gameObject.SetActive(false);
    }

    IEnumerator FadeOut(SpriteRenderer render, float duration, float newAlpha)
    {
        yield return new WaitForSeconds(tooltipDuration);
        float originalAlpha = render.color.a;
        float elapsed = 0f;

        while (elapsed < duration)
        {
            elapsed += Time.deltaTime;
            float alpha = Mathf.Lerp(originalAlpha, newAlpha, elapsed / duration);
            render.color = new Color(render.color.r, render.color.g, render.color.b, alpha);
            yield return null;
        }
        gameObject.SetActive(false);
    }
}
