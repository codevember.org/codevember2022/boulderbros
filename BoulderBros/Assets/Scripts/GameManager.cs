using System.Collections;
//using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
using UnityEngine.Serialization;
using Unity.Netcode;

// using UnityEngine.Serialization;

public class GameManager : MonoBehaviour
{

    public static GameManager Instance { get; private set; }

    [Header("Performance")]
    [SerializeField] private int fpsLimit = 60;

    [Header("Debug/Flags")]
    [SerializeField] private bool timeFreeze;
    [SerializeField] private bool skipMainMenu;
    

    [Header("References/Managers")]
    [SerializeField] private InputController inputController;
    [SerializeField] private PlayerManager playerManager;
    [SerializeField] private HeightUpdater heightUpdater;
    [SerializeField] private RopeManager ropeManager;
    
    
    [Header("References/Controllers")]
    public PlayerController playerController1;
    public PlayerController playerController2;
    
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

// #if UNITY_EDITOR
//         //QualitySettings.vSyncCount = 0;  // VSync must be disabled
//         Application.targetFrameRate = fpsLimit;
// #endif
    }

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        Debug.Log("OnSceneLoaded: " + scene.name); 
    }

    // Start is called before the first frame update
    void Start()
    {
        MenuManager.Instance.EnableMainMenu();
        //AudioStuff
        AudioManager.Instance.Play("frozenMusic");
        AudioManager.Instance.Play("forestAmbience");
        
        //Debug:
        if(skipMainMenu)
            DebugStartGame();
    }

    #region GameStates
    //Toggle all UI, flip player 2 to the right, 
    // ReSharper disable Unity.PerformanceAnalysis
    public void StartGame()
    {
        MenuManager.Instance.DisableMainMenu();
        MenuManager.Instance.SetInGameUIEnabled(true);
        playerManager.ResetPlayersToStart();
        heightUpdater.ResetInitialHeightOffset();
        CameraManager.Instance.GetCameraFollowPoints().SetCameraPosition(0);
    }

    private void DebugStartGame()
    {
        MenuManager.Instance.DisableMainMenu();
        MenuManager.Instance.SetInGameUIEnabled(true);
        CameraManager.Instance.SetCameraFollowMode(CameraFollowType.followConstant);
        RestartGame();
    }
    
    public void StartGameTransition()
    {
        UIManager.Instance.GetSceneTransitionManager().TransitionScene(StartGame);
    }

    private void RestartGame()
    {
        UIManager.Instance.RestartGameUI();
        playerManager.ResetPlayers();
        CameraManager.Instance.GetCameraFollowPoints().SetCameraPosition(0);
    }

    public void RestartGameTransition()
    {
        UIManager.Instance.GetSceneTransitionManager().TransitionScene(RestartGame);
    }

    public void GameOver()
    {
        playerManager.DisablePlayers();
        UIManager.Instance.GameOverScreen();
    }

    public void PauseGame()
    {
        if (!MenuManager.Instance.GetMainMenuEnabled())
        {
            Debug.Log("Should pause game in gameManager");
            MenuManager.Instance.TogglePauseMenuEnabled();
            UIManager.Instance.TogglePauseUI();
        }
    }

    public void ToggleTimeFreeze()
    {
        if (timeFreeze)
        {
            Debug.Log("Toggling Time Freeze");
            if (Time.timeScale == 0)
            {
                Time.timeScale = 1;
            }
            else
            {
                Time.timeScale = 0;
            }
        }
    }

    #endregion

    #region Getters

    public InputController GetInputController()
    {
        return inputController;
    }

    public PlayerManager GetPlayerManager()
    {
        return playerManager;
    }

    public HeightUpdater GetHeightManager()
    {
        return heightUpdater;
    }

    public RopeManager GetRopeManager()
    {
        return ropeManager;
    }

    #endregion
}
