using System;
using System.Net;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.Serialization;

public class PlayerStats : NetworkBehaviour

{
	[Header("References")] 
	[SerializeField] private PlayerController playerController;

	[Header("Stats/Stamina")] 
	[SerializeField] private float minStamina = 25f;			//defines the minimum stamina after being exhausted to be able to run/Climb again if stamina reached 0
    [SerializeField] private float maxStamina = 100f;
    [SerializeField] private float currentStamina = 100f;
    [SerializeField] private float staminaLoss = 10f;
    [SerializeField] private float staminaRegen = 15f;
    
    [Header("Stats/StaminaFactors")]
    [SerializeField] private float staminaLossRun = 10f;
    [SerializeField] private float staminaLossClimbIdle = 1f;
    [SerializeField] private float staminaLossClimb = 3f;
    [SerializeField] private float staminaLossOverhangFactor = 1.25f;
    [SerializeField] private float staminaLossUpsideDownFactor = 1.75f;
    [SerializeField] private float staminaLossRopeBroIdle = 5f;
    [SerializeField] private float staminaLossRopeBro = 7f;
    
    [Header("StaminaCosts")]
    [SerializeField] private float jumpStaminaCost = 33;
    [SerializeField] private bool staminaDepleted;

    [Header("Exhaustion")] 
    [SerializeField] private bool exhaustBreak;
    public event Action OnExhaustionStateChanged;
    
    [Header("Height Warning")]
    [SerializeField] private float currentHeight;
    [SerializeField] private float dangerHeight = 5f;
    
    [field:SerializeField] public bool warnedAlready { get; private set;}

    [Header("Distance")] 
    [SerializeField] private float currentMaxDistance;
    
    
    
    // Start is called once before the first execution of Update after the MonoBehaviour is created
    void Start()
    {
        ResetStamina();
    }

    private void Update()
    {
	    //if its online multiplayer and not the owner, dont do shit
	    if(NetworkController.Instance.onlineMultiplayer && !IsOwner) return;
	    
	    UpdateStamina();
	    UpdateCurrentHeight();
    }

    #region Stamina
    public void ResetStamina()
    {
	    currentStamina = maxStamina;
    }

    private void IsExhausted()
    {
	    if (playerController.isExhausted && playerController.isGrounded && !playerController.isClimbing)
	    {
		    if (!exhaustBreak)
		    {
			    exhaustBreak = true;
			    playerController.GetPlayerInput().SetPlayerInputEnabled(false);
			    playerController.GetPlayerMovement().FreezeMovement();
			    playerController.GetPlayerAnimator().SetExhaustAnimation(true);
		    }
	    }
    }

    private void IsNotExhausted()
    {
	    if (exhaustBreak)
	    {
		    exhaustBreak = false;
		    playerController.GetPlayerInput().SetPlayerInputEnabled(true);
		    playerController.GetPlayerMovement().ResetMovement();
		    playerController.GetPlayerAnimator().SetExhaustAnimation(false);
	    }
    }
    
    private void UpdateStamina()
    {
	    if (currentStamina > maxStamina)
		    currentStamina = maxStamina;

	    else if ((playerController.isClimbing || playerController.isRunning) && currentStamina > 0.1f)
	    {
		    currentStamina -= staminaLoss * Time.deltaTime;
	    }
	    else if (currentStamina < maxStamina)
	    {
		    currentStamina += staminaRegen * Time.deltaTime;
	    }

	    if (currentStamina > 0.1f)
	    {
		    playerController.hasStamina = true;
		    
		    if (currentStamina >= minStamina)
		    {
			    staminaDepleted = false;
			    playerController.isExhausted = false;
			    IsNotExhausted();
		    }
	    }
	    else
	    {
		    playerController.hasStamina = false;
		    playerController.isExhausted = true;
		    staminaDepleted = true;
		    IsExhausted();
	    }

	    playerController.GetPlayerAnimator().UpdateFaceColor(currentStamina);
	    playerController.GetPlayerAnimator().UpdateFaceExpression(currentStamina, playerController.GetPlayerMovement().GetFallOrSlideOrDrag());
	    UIManager.Instance.UpdateStaminaUI(playerController.GetStaminaMaterial(), playerController);
    }

    public void ControlStaminaLoss()
    {
	    PlayerRope playerRope = playerController.GetPlayerRope();
	    if (!playerController.isClimbing)
	    {
		    if(playerController.isRunning)
			    SetStaminaLoss(staminaLossRun);
		    else
			    SetStaminaLoss(staminaLossClimb);
	    }
	    else
	    {
		    if (playerController.climbIdle)
		    {
			    if (playerRope.BroOnRope())
			    {
				    SetStaminaLoss(staminaLossRopeBroIdle);
			    }
			    else
			    {
				    SetStaminaLoss(staminaLossClimbIdle);
			    }
		    }
		    else
		    {
			    if (playerRope.BroOnRope())
			    {
				    SetStaminaLoss(staminaLossRopeBro);
			    }
			    else
			    {
				    SetStaminaLoss(staminaLossClimb);
			    }
		    }
	    }
    }

    private void SetStaminaLoss(float value)
    {
	    if(!playerController.onOverhang && !playerController.isUpsideDown)
			staminaLoss = value;
	    if (playerController.onOverhang && !playerController.isUpsideDown)
		    staminaLoss = staminaLossOverhangFactor * value;
	    if(playerController.isUpsideDown)
		    staminaLoss = staminaLossUpsideDownFactor * value;
    }

    public void StaminaBurnOnce(float staminaCost)
    {
	    currentStamina -= staminaCost;
    }

    public void StaminaBurnJump(float staminaCost, float jumpTime)
    {
	    var staminaBurn = staminaCost * jumpTime * 2f;
	    //Debug.Log("JumpCost: " + staminaBurn);
	    currentStamina -= staminaBurn;
		    
    }

    public float GetCurrentStamina()
    {
	    return currentStamina;
    }
    
    public float GetMaxStamina()
    {
	    return maxStamina;
    }

    public float GetJumpStaminaCosts()
    {
	    return jumpStaminaCost;
    }

    public bool GetStaminaDepleted()
    {
	    return staminaDepleted;
    }
    
    #endregion
    
    #region Height
        private void UpdateCurrentHeight()
        {
    		Vector3 groundZero = new Vector3(transform.position.x-0.01f, transform.position.y, transform.position.z);
    		Debug.DrawRay(groundZero, Vector3.up * -100, Color.black);
    		RaycastHit2D getHeight = Physics2D.Raycast(groundZero, Vector3.up * -1f, 100, playerController.GetGroundCheck().GetGroundLayerMask());
    
    		if (getHeight)
            {
    			//Debug.Log("Height raycast hit");
    			currentHeight = getHeight.distance;
            }
    
    		if (currentHeight > dangerHeight)
    		{
    			//Debug.Log("Exceeded danger height for " + player);
    			UIManager.Instance.TriggerHeightWarning(playerController, true);
    			UIManager.Instance.SetFallColor(true);
    			warnedAlready = true;
    		}
            else
            {
    			UIManager.Instance.SetFallColor(false);
    			warnedAlready = false;
    		}
    			
    	}
    
    	public float GetCurrentHeight()
    	{
    		return currentHeight;
    	}
    	#endregion
	    
	#region Distance
	public void SetCurrentMaxDistance(float value)
	{
		currentMaxDistance = value;
	}
	public float GetCurrentMaxDistance()
	{
		return currentMaxDistance;
	}
	#endregion
}

