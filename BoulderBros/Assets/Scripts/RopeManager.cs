// using System.Collections;
// using System.Collections.Generic;
using UnityEngine;

public class RopeManager : MonoBehaviour
{
    [SerializeField] private GameObject ropeParent;
    [SerializeField]private GameObject[] ropeGameObjects;
    [SerializeField]private RopeController[] ropeControllers;
    // Start is called before the first frame update
    void Start()
    {
        ropeGameObjects = new GameObject[ropeParent.transform.childCount];
        ropeControllers = new RopeController[ropeGameObjects.Length];
        CacheRopeControllers();
    }

    private void CacheRopeControllers()
    {
        for(int i = 0; i< ropeGameObjects.Length; i++)
        {
            ropeGameObjects[i] = ropeParent.transform.GetChild(i).gameObject;
            if(ropeGameObjects[i]!= null)
                ropeControllers[i] = ropeGameObjects[i].GetComponent<RopeController>();
        }
    }

    public RopeController[] GetRopeControllers()
    {
        return ropeControllers;
    }
}
