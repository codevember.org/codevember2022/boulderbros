//using System.Collections;
//using System.Collections.Generic;

using System;
using UnityEngine;
using Rewired;

public class InputController : MonoBehaviour
{
    [Header("References")]
    [SerializeField] private PlayerController playerController1;
    [SerializeField] private PlayerController playerController2;

    [Header("Information")] 
    private Controller inputControllerPlayer1;
    [SerializeField] private ControllerType inputDevicePlayer1;
    [SerializeField] private String inputDeviceNamePlayer1;
    private Controller inputControllerPlayer2;
    [SerializeField] private ControllerType inputDevicePlayer2;
    [SerializeField] private String inputDeviceNamePlayer2;
    
    private void Start()
    {
        GetInputDevices();
    }

    private void Update()
    {
        GetInputDevices();
    }
    
    public void GetInputDevices()
    {
        if (playerController1.gameObject.activeSelf)
        {
            if (playerController1.rewiredPlayer.controllers.GetLastActiveController() != null)
            {
                inputControllerPlayer1 = playerController1.rewiredPlayer.controllers.GetLastActiveController();
                inputDevicePlayer1 = inputControllerPlayer1.type;
                inputDeviceNamePlayer1 = inputControllerPlayer1.name;
            }
        }

        if (playerController2.gameObject.activeSelf)
        {
            if (playerController2.rewiredPlayer.controllers.GetLastActiveController() != null)
            {
                inputControllerPlayer2 = playerController2.rewiredPlayer.controllers.GetLastActiveController();
                inputDevicePlayer2 = inputControllerPlayer2.type;
                inputDeviceNamePlayer2 = inputControllerPlayer2.name;
            }
        }
    }
    
    public bool GetJoystickConnected()
    {
        if (playerController1.rewiredPlayer.controllers.Joysticks.Count > 0)
            return true;
        return false;
    }

    public String GetInputDeviceNamePlayer1()
    {
        return inputDeviceNamePlayer1;
    }
    
    public String GetInputDeviceNamePlayer2()
    {
        return inputDeviceNamePlayer2;
    }

    public ControllerType GetInputDevicePlayer1()
    {
        return inputDevicePlayer1;
    }
    
    public ControllerType GetInputDevicePlayer2()
    {
        return inputDevicePlayer2;
    }
}
