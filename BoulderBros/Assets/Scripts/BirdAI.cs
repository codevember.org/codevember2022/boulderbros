using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdAI : MonoBehaviour
{

    [SerializeField] private BirdAnimator birdAnimator;
    [SerializeField] private CircleCollider2D birdCircleTrigger;
    [SerializeField] private LoopMovement birdMovementAnimation;
    [SerializeField] private bool shouldFlyAway = false;
    [SerializeField] private float flyOffset = 0.417f;
    [SerializeField] private bool isFlying = false;

    // Start is called before the first frame update
    public void OnTriggerEnter2D(Collider2D other)
    {
        if (shouldFlyAway)
        {
            if (!isFlying)
            {
                if (other.gameObject.layer == LayerMask.NameToLayer("CameraTarget"))
                {
                    if (other.gameObject.name != "CameraMidPoint")
                    {
                        isFlying = true;
                        Debug.Log(other.name + "got into Trigger - start flying away");
                        CheckDirection();
                        birdAnimator.StartFlyAnimation();
                        StartCoroutine(StartFlying());
                    }
                }
            }
        }
    }

    private void CheckDirection()
    {
        if(transform.position.x < birdMovementAnimation.GetTargetPosX())
        {
            birdAnimator.FlipBird();
        }
    }
    IEnumerator StartFlying()
    {
        yield return new WaitForSeconds(flyOffset);
        birdMovementAnimation.enabled = true;
    }
}
