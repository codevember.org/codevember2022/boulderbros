using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstantMovement : MonoBehaviour
{
    [SerializeField] private bool visible;
    [SerializeField] private bool useMovementVector = false;
    [SerializeField] private Vector2 movementVector = new Vector3(0,0,0);
    [SerializeField] private float constantSpeed = 0.0005f;
    // Start is called before the first frame update

    private void Start()
    {
        
    }

    private void OnBecameVisible()
    {
        visible = true;
        if (transform.rotation.y == 180)
            movementVector = new Vector3(-movementVector.x, movementVector.y, 0);
    }

    private void OnBecameInvisible()
    {
        visible = false;
    }

    private void FixedUpdate()
    {
        if (visible)
        {
            if (!useMovementVector)
                transform.Translate(Vector3.right * constantSpeed);
            else
            {
                transform.Translate(movementVector);
                Debug.DrawRay(transform.position, movementVector * 100 , Color.black);
            }
        }       
    }
}
