using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UIElements;


public class PlayerEffectManager : MonoBehaviour
{
    [Header("References")] 
    [SerializeField] private PlayerController playerController;
    [SerializeField] private PlayerAnimator playerAnimator;
    
    [Header("Cold Breath Stuff")]
    [SerializeField] private GameObject coldBreath;
    [SerializeField] private Animator coldBreathAnimator;

    [Header("GroundParticle Stuff")] 
    [SerializeField] private bool groundParticlesPlaying;
    [SerializeField] private ParticleSystem[] groundParticles;
    [SerializeField] private ParticleSystem dustPS;
    [SerializeField] private int dustEmitAmount;
    [SerializeField] private ParticleSystem leavePS;
    [SerializeField] private int leaveEmitAmount;
    [SerializeField] private ParticleSystem snowPS;
    [SerializeField] private int snowEmitAmount;
    [SerializeField] private GroundCheck.GroundType currentGroundType;
    private GroundCheck playerGroundCheck;
    

    private void Start()
    {
        playerGroundCheck = playerController.GetComponent<GroundCheck>();
        playerGroundCheck.OnGroundTypeChanged += UpdateCurrentGroundType;
        playerGroundCheck.OnGroundTypeChanged += ToggleGroundParticles;
    }

    private void UpdateCurrentGroundType()
    {
        currentGroundType = playerGroundCheck.GetGroundType();
    }
    
    public void SetColdBreath(bool value)
    {
        if (coldBreath.activeSelf != value)
        {
            if (value)
            {
                coldBreath.SetActive(true);
            }
            else
            {
                //only set false if animation is finished
                if (coldBreathAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime > 1)
                {
                    //Debug.Log(coldBreathAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime);
                    coldBreath.SetActive(false);
                }
            }
        }          
    }
    
    public void FlipDust()
    {
        //Debug.Log("Flip Dust");
        var main = dustPS.main;
        main.startRotationY = playerController.facingRight ? 0 : 180;
        
        if(playerController.isRunning)
            EmitGroundParticles();
    }

    public void EmitGroundParticles()
    {
        switch (currentGroundType)
        {
            case GroundCheck.GroundType.untagged:
                dustPS.Emit(dustEmitAmount);
                break;
            
            case GroundCheck.GroundType.grass:
                leavePS.Emit(leaveEmitAmount);
                break;
            
            case GroundCheck.GroundType.wood:
                dustPS.Emit(dustEmitAmount);
                break;
            
            case GroundCheck.GroundType.stone:
                dustPS.Emit(dustEmitAmount);
                break;
            
            case GroundCheck.GroundType.snow:
                snowPS.Emit(snowEmitAmount);
                break;
            
            case GroundCheck.GroundType.ice:
                dustPS.Emit(dustEmitAmount);
                break;
        }
    }

    public void ToggleGroundParticles()
    {
        if (!IsGroundParticleGroundType())
        {
            StopAllGroundParticles();
        }
        else
        {
            switch (currentGroundType)
            {
                case GroundCheck.GroundType.grass:
                    GroundParticleToggle(leavePS);
                    break;

                case GroundCheck.GroundType.wood:
                    StopAllGroundParticles();
                    //GroundParticleToggle(woodPS);
                    break;

                case GroundCheck.GroundType.stone:
                    //StopAllGroundParticles();
                    //GroundParticleToggle(stonePS);
                    break;
                
                case GroundCheck.GroundType.snow:
                    //StopAllGroundParticles();
                    GroundParticleToggle(snowPS);
                    break;

                case GroundCheck.GroundType.ice:
                    StopAllGroundParticles();
                    //GroundParticleToggle(icePS);
                    break;
            }
        }
    }

    private void StopAllGroundParticles()
    {
        foreach (ParticleSystem ps in groundParticles)
        {
            ps.Stop();
        }
        groundParticlesPlaying = false;
    }

    private void GroundParticleToggle(ParticleSystem ps)
    {
        if (!groundParticlesPlaying)
        {
            if (playerController.isRunning)
            {
                groundParticlesPlaying = true;
                ps.Play();
            }
        }
        else
        {
            ps.Stop();
            groundParticlesPlaying = false;
        }
    }
    
    private bool IsGroundParticleGroundType()
    {
        switch (currentGroundType)
        {
            case GroundCheck.GroundType.untagged:
                return false;
            
            case GroundCheck.GroundType.grass:
                return true;
            
            case GroundCheck.GroundType.wood:
                return true;
            
            case GroundCheck.GroundType.stone:
                return false;
            
            case GroundCheck.GroundType.snow:
                return true;
            
            case GroundCheck.GroundType.ice:
                return true;
        }

        return false;
    }

    public void EmitGroundParticlesDelayed(float delayTime)
    {
        StartCoroutine(EmitDelay(delayTime));
    }
    
    private IEnumerator EmitDelay(float delayTime)
    {
        yield return new WaitForSeconds(delayTime);
        EmitGroundParticles();
    }
}
