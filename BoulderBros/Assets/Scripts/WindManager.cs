using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindManager : MonoBehaviour
{
    public static WindManager Instance { get; private set; }

    [SerializeField] private ParticleSystem windParticle;
    [SerializeField] private ParticleSystem windSwirlParticle;
    [Range(0f,7f)]
    [SerializeField] private float windSpeed;
    [SerializeField] private float emissionWindParticle;
    [SerializeField] private float emissionWindSwirlParticle;
    ParticleSystem.VelocityOverLifetimeModule velModule;
    ParticleSystem.VelocityOverLifetimeModule velModuleSwirl;
    ParticleSystem.EmissionModule emissionModule;
    ParticleSystem.EmissionModule emissionmoduleSwirl;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void ChangeWind()
    {
        velModule = windParticle.velocityOverLifetime;
        velModuleSwirl = windSwirlParticle.velocityOverLifetime;
        velModule.xMultiplier = velModuleSwirl.xMultiplier = windSpeed;
        velModule.zMultiplier = velModuleSwirl.zMultiplier = windSpeed;
    }

    public void ChangeWindEmission()
    {
        emissionModule = windParticle.emission;
        emissionmoduleSwirl = windSwirlParticle.emission;
        emissionModule.rateOverTime = emissionWindParticle;
        emissionmoduleSwirl.rateOverTime = emissionWindSwirlParticle;
    }

    private void OnValidate()
    {
        ChangeWind();
        ChangeWindEmission();
    }
}
