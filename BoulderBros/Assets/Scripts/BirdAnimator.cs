using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdAnimator : MonoBehaviour
{
    [SerializeField] private Animator animator;
    [SerializeField] private SpriteRenderer[] birdSprites;
    [SerializeField] private AnimationClip[] uniqueIdleClips;  // Array of unique idle animation clips
    [SerializeField] private bool isFlying = false;

    // Variables to control the random time interval for the unique idle animation
    [Header("Time Interval for Unique Idle Animation")]
    [Tooltip("Min and Max time (in seconds) between unique idle animations")]
    [SerializeField] private Vector2 idleSwitchMinMaxTime;

    [SerializeField] private float idleSwitchTimer;

    void Start()
    {
        StartCoroutine(PlayIdleAnimations());
    }

    IEnumerator PlayIdleAnimations()
    {
        while (!isFlying)
        {
            idleSwitchTimer = Random.Range(idleSwitchMinMaxTime.x, idleSwitchMinMaxTime.y);
            yield return new WaitForSeconds(idleSwitchTimer);

            // Choose a random unique idle animation from the array
            int randomIndex = Random.Range(0, uniqueIdleClips.Length);
            AnimationClip chosenClip = uniqueIdleClips[randomIndex];
            animator.SetInteger("idleIndex", randomIndex);

            animator.SetTrigger("PlayUniqueIdle");

            yield return new WaitForSeconds(chosenClip.length);

            animator.SetTrigger("PlayRegularIdle");
            animator.SetInteger("idleIndex", -1);
        }
    }

    
    public void StartFlyAnimation()
    {
        // Stop the idle animation coroutine when flying
        isFlying = true;
        StopCoroutine(PlayIdleAnimations());

        // Trigger fly animation in the Animator
        animator.SetTrigger("fly");

        // Reset idle-related triggers to prevent accidental switching
        animator.ResetTrigger("PlayRegularIdle");
        animator.ResetTrigger("PlayUniqueIdle");
    }

    public void FlipBirdSprites()
    {
        foreach (SpriteRenderer render in birdSprites)
        {
            if (!render.flipX)
            {
                render.flipX = true;
            }
        }
    }

    public void FlipBird()
    {
        if (transform.eulerAngles.y == 0)
        {
            transform.eulerAngles = new Vector3(0, 180, 0);
        }
        else
            transform.eulerAngles = new Vector3(0, 0, 0);
    }
}
