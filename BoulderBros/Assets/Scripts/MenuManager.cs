using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : MonoBehaviour
{
    [Header("References")] 
    //[SerializeField] private PlayerController playerController1;
    //[SerializeField] private PlayerController playerController2;
    [Header("Menu Flags")]
    [SerializeField] private bool mainMenuEnabled = true;
    [SerializeField] private bool ingameUIEnabled;
    [SerializeField] private bool pauseMenuEnabled;
    
    public static MenuManager Instance { get; private set; }

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        //playerController1 = GameManager.Instance.GetPlayerManager().GetPlayerController1();
        //playerController2 = GameManager.Instance.GetPlayerManager().GetPlayerController2();
    }

    public void EnableMainMenu()
    {
        mainMenuEnabled = true;
        //Campfire grill animation for players - not needed right now but could be used somewhere else
        
        // playerController1.playerAnimator.StartPlayerMainMenuAnimation(true);
        // playerController2.playerAnimator.StartPlayerMainMenuAnimation(true);
        GameManager.Instance.GetPlayerManager().SetPlayersInputEnabled(false);
        CameraManager.Instance.SetCameraFollowMode(CameraFollowType.followPoints);
        MenuParallaxManager.Instance.SetParallaxLayers(ParallaxType.mainMenu, true);
        UIManager.Instance.ToggleMainMenuEnvironment(true);
        UIManager.Instance.ToggleIngameEnvironment(false);
        UIManager.Instance.ToggleMainMenuUI(true); 
        UIManager.Instance.ToggleIngameUI(false);
    }
    
    public void DisableMainMenu()
    {
        mainMenuEnabled = false;
        GameManager.Instance.GetPlayerManager().SetPlayersInputEnabled(true);
        UIManager.Instance.ToggleMainMenuEnvironment(false);
        UIManager.Instance.ToggleIngameEnvironment(true);
        UIManager.Instance.ToggleMainMenuUI(false);  
        UIManager.Instance.ToggleIngameUI(true);
        //CameraManager.Instance.SetCameraFollowMode(CameraFollowType.followPoints);
        MenuParallaxManager.Instance.SetParallaxLayers(ParallaxType.ingame, true);
    }

    public void SetMainMenuEnabled(bool set)
    {
        mainMenuEnabled = set;
    }

    public void SetInGameUIEnabled(bool set)
    {
        ingameUIEnabled = set;
    }

    public void SetPauseMenuEnabled(bool set)
    {
        pauseMenuEnabled = set;
    }

    public void TogglePauseMenuEnabled()
    {
        pauseMenuEnabled = !pauseMenuEnabled;
        Debug.Log("pause Menu enabled is:" + pauseMenuEnabled);
    }
    
    public bool GetMainMenuEnabled()
    {
        return mainMenuEnabled;
    }
    
    public bool GetIngameUIEnabled()
    {
        return ingameUIEnabled;
    }
    
    public bool GetPauseMenuEnabled()
    {
        return pauseMenuEnabled;
    }
}
