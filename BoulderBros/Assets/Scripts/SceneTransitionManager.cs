using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SceneTransitionManager : MonoBehaviour
{
    [Header("Scene Transition Animation")]
    [SerializeField] private GameObject sceneTransitionGO;
    [SerializeField] private AnimationClip sceneTransitionMountainClip;
    [SerializeField] private AnimationClip currentSceneTransitionClip;
    
    public void TransitionScene(UnityAction action)
    {
        StartCoroutine(SceneTransition(action));
    }
    
    public void PlaySceneTransition()
    {
        sceneTransitionGO.SetActive(true);
        Invoke(nameof(DeactivateIntroObject), sceneTransitionMountainClip.length);
    }
    
    public void DeactivateIntroObject()
    {
        sceneTransitionGO.SetActive(false);
    }

    private IEnumerator SceneTransition(UnityAction action)
    {
        PlaySceneTransition();
        yield return new WaitForSeconds(GetCurrentSceneTransitionLength()/2f);
        action.Invoke();
    }
    
    public float GetCurrentSceneTransitionLength()
    {
        return currentSceneTransitionClip.length;
    }
}
