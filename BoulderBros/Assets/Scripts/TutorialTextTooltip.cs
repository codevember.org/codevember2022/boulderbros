using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TutorialTextTooltip : TutorialTooltip
{

    [SerializeField] private GameObject tutorialText;
    [SerializeField] private TextMeshProUGUI tutorialTextmesh;


    void OnEnable()
    {
        CheckInputDevice();
        //This will be called when object is enabled.
        Debug.Log("OnEnabled()", gameObject);
        Fade(tutorialTextmesh, fadeDuration, 0f);
    }
}
