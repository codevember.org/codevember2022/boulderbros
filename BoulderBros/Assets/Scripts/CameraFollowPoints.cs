﻿// using System.Collections;
// using System.Collections.Generic;
using UnityEngine;

public class CameraFollowPoints : MonoBehaviour
{
	public Transform[] camPos;
	public Transform[] volumeTriggers;
	[SerializeField] public int currentPos;
	[SerializeField] private int targetCamPos;
	[SerializeField] private Transform midPoint;
	private float timeElapsed;
	public float lerpDuration = 3;
	Vector3 startPosition;
	Vector3 targetPosition;

	public bool shouldLerp;


	//public Transform target;
	public Transform mainCam;

    private void OnEnable()
    {
        PlayerTriggeredCamVolume(currentPos);
        //Debug.Log("Was enabled");
    }

    void Update()
    {
        if (shouldLerp)
        {
			if (timeElapsed < lerpDuration)
			{
				transform.position = Vector3.Lerp(startPosition, targetPosition, timeElapsed / lerpDuration);
				timeElapsed += Time.deltaTime;
			}
            else
            {
				timeElapsed = 0;
				shouldLerp = false;
				currentPos = targetCamPos;
			}		
		}

		midPoint.position = CameraManager.Instance.GetMidPointPosition();
    }

    public void PlayerTriggeredCamVolume(int index)
    {
        if (!shouldLerp)
        {
                if (currentPos != index)
                {
                    SetCameraPosition(index);
                }
        }  
    }

    // ReSharper disable Unity.PerformanceAnalysis
    public void SetTargetCamPos(int value)
    {
		if (value < camPos.Length)
        {
			targetCamPos = value;
		}
        else
        {
			Debug.Log("Already arrived at last cam pos");
        }
    }

    public void SetCameraPosition(int value)
    {
		SetTargetCamPos(value);
		startPosition = transform.position;
		targetPosition = camPos[targetCamPos].position;
		shouldLerp = true;
    }

	public void ResetCurrentPosition()
    {
        currentPos = -1;
        shouldLerp = false;
    }
    
}
