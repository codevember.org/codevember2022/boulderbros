// using System.Collections;
// using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;

public class ButtonManager : MonoBehaviour
{
    public void StartGameButton()
    {
        UIManager.Instance.GetSceneTransitionManager().TransitionScene(GameManager.Instance.StartGame);
    }

    public void ResumeGameButton()
    {
        
    }

    public void ContinueGameButton()
    {
        
    }

    public void SettingsButton()
    {
        
    }

    public void StartServer()
    {
        NetworkManager.Singleton.StartServer();
        Debug.Log("Starting Server");
    }

    public void StartHost()
    {
        NetworkManager.Singleton.StartHost();
        Debug.Log("Starting Host");
    }

    public void StartClient()
    {
        NetworkManager.Singleton.StartClient();
        Debug.Log("Starting Client");
    }
    
    
}
