using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ParallaxType
{
    mainMenu,
    ingame
}

public class MenuParallaxManager : MonoBehaviour
{
    [SerializeField] private MenuParallax[] mainMenuParallaxLayers;
    [SerializeField] private MenuParallax[] ingameParallaxLayers;
    [SerializeField] private MenuParallax[] currentParallaxLayers;
    [SerializeField] private float[] parallaxLayerIntensity;
    [SerializeField] private bool parallaxOnlyY = false;
    public bool invertInput = false;
    [SerializeField] private bool changedIntensity = false;

    public static MenuParallaxManager Instance;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }

    }

    // Start is called before the first frame update
    void Start()
    {
        currentParallaxLayers = mainMenuParallaxLayers;
    }

    // Update is called once per frame
    void Update()
    {
        if (!changedIntensity)
            ChangeParallaxLayerIntensity();
    }

    private void OnValidate()
    {
        changedIntensity = false;
    }
    private void ChangeParallaxLayerIntensity()
    {
        for(int i = 0; i < mainMenuParallaxLayers.Length; i++)
        {
            currentParallaxLayers[i].SetOffSetMultiplier(parallaxLayerIntensity[i]);
            currentParallaxLayers[i].SetParallaxOnlyY(parallaxOnlyY);
        }
        changedIntensity = true;
        Debug.Log("Just changed parallax intensities");
    }

    public void SetParallaxLayers(ParallaxType parallaxType, bool toggle)
    {
        if(parallaxType == ParallaxType.mainMenu)
        {
            currentParallaxLayers = mainMenuParallaxLayers;
            foreach(MenuParallax mP in mainMenuParallaxLayers)
            {
                mP.enabled = toggle;
            }
        }
        if (parallaxType == ParallaxType.ingame)
        {
            currentParallaxLayers = ingameParallaxLayers;
            foreach (MenuParallax mP in ingameParallaxLayers)
            {
                mP.enabled = toggle;
            }
        }
    }
}
