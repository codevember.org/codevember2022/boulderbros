using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RopeSegment : MonoBehaviour
{
    [SerializeField] private RopeController ropeControllerParent;
    public GameObject connectedAbove, connectedBelow, secondConnected;
    [SerializeField] private DistanceJoint2D segmentDistanceJoint;
    [SerializeField] private HingeJoint2D segmentHingeJoint;
    [SerializeField] private HingeJoint2D[] hingeJoints;
    [SerializeField] private BoxCollider2D col2D;
    [SerializeField] private Transform ropeRendererTransform;
    [SerializeField] private bool isStartSegment = false;
    [SerializeField] private bool isEndSegment = false;
    [SerializeField] private float ropeRendererGapFix = 1.05f;
    public float currentSegmentSize;
    [SerializeField] private float maxSegmentDistance = 0.15f;
    [SerializeField] private Vector2 initialConnectionOffset;
    

    // Start is called before the first frame update
    void Start()
    {
        col2D = gameObject.GetComponent<BoxCollider2D>();
        hingeJoints = gameObject.GetComponents<HingeJoint2D>();
        segmentDistanceJoint = gameObject.GetComponent<DistanceJoint2D>();
        SetSegmentHingeJoint();
        
        connectedAbove = GetComponent<HingeJoint2D>().connectedBody.gameObject;
        RopeSegment aboveSegment = connectedAbove.GetComponent<RopeSegment>();
        SetSegmentSize(currentSegmentSize);
    }
    
    public void SetSegmentSize(float value)
    {
        //transform.localScale = new Vector3(transform.localScale.x, value, transform.localScale.z);
        SetSegmentHingeJoint();
        SetSegmentColliderSize(value);
        //if(ropeControllerParent.GetRopeType() == RopeController.RopeType.attached)
        if (Application.isEditor && !Application.isPlaying)
            SetSegmentPosition();
        SetSegmentDistance(value);
        SetSegmentAnchors();
        SetSegmentRendererSize(value*16f);
    }

    private void SetSegmentHingeJoint()
    {
        if (hingeJoints.Length > 1)
        {
            foreach (HingeJoint2D joint in hingeJoints)
            {
                if (!joint.connectedBody.gameObject.name.Contains("Player"))
                    segmentHingeJoint = joint;
            }
        }
    }

    private void SetSegmentColliderSize(float value)
    {
        col2D.size = new Vector2(col2D.size.x, value);
        col2D.offset = new Vector2(0, -value / 2f);
    }

    private void SetSegmentRendererSize(float value)
    {
        ropeRendererTransform.localScale = new Vector3(ropeRendererTransform.localScale.x, (value / 2f)*ropeRendererGapFix, ropeRendererTransform.localScale.z);
    }

    public void SetRopeRendererGapFix(float value)
    {
        ropeRendererGapFix = value;
    }

    private void SetSegmentDistance(float newMaxDistance)
    {
        maxSegmentDistance = newMaxDistance;
        if (segmentDistanceJoint != null)
            segmentDistanceJoint.distance = maxSegmentDistance;
    }

    //Check if Start or Endsegment: For startsegment change nothing, for endsegment only change the hingeJoint that doesnt contain a player connectedBody
    //for everything else (also distancejoint of endsegment) change the anchor of the hingejoint
    private void SetSegmentAnchors()
    {
        if (!isStartSegment)
        {
            segmentDistanceJoint.connectedAnchor = new Vector2(0f, -col2D.size.y);
            if (!isEndSegment)
            {
                segmentHingeJoint.connectedAnchor = new Vector2(0f, -col2D.size.y);
            }
            else
            {
                foreach (HingeJoint2D joint in hingeJoints)
                {
                    if (!joint.connectedBody.gameObject.name.Contains("Player"))
                    {
                        //Debug.Log("changing hingeJoint:" + gameObject.name + " " + joint.connectedBody.gameObject.name);
                        segmentHingeJoint = joint;
                        joint.connectedAnchor = new Vector2(0f, -col2D.size.y);
                        
                    }
                }
            }
        }
    }

    private void SetSegmentPosition()
    {
        if (!isStartSegment)
            transform.localPosition = new Vector3(transform.localPosition.x, (segmentHingeJoint.connectedBody.transform.localPosition.y - col2D.size.y), transform.localPosition.z);
    }

    public float GetSegmentMaxDistance()
    {
        return maxSegmentDistance;
    }

    public HingeJoint2D getSegmentHingeJoint()
    {
        return segmentHingeJoint;
    }

    public HingeJoint2D GetPlayerEndHingeJoint()
    {
        HingeJoint2D playerEndJoint = new HingeJoint2D();
        foreach(HingeJoint2D joint in hingeJoints)
        {
            if (joint.connectedBody.gameObject.name.Contains("Player"))
            {
                playerEndJoint = joint;
            }
        }
        return playerEndJoint;
    }

    public HingeJoint2D GetRopeEndHingeJoint()
    {
        HingeJoint2D ropeEndJoint = new HingeJoint2D();
        foreach (HingeJoint2D joint in hingeJoints)
        {
            if (!joint.connectedBody.gameObject.name.Contains("Player"))
            {
                ropeEndJoint = joint;
            }
        }
        return ropeEndJoint;
    }
}
