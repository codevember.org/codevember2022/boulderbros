using System;
// using System.Collections;
// using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;
using Rewired;

public class PlayerInput : NetworkBehaviour
{
    [Header("Input Stuff")]
    [SerializeField] private Vector2 movementInput;
    [SerializeField] private float menuInput;
    [SerializeField] private Vector2 cameraInput;
    [SerializeField] private bool inputEnabled;
    
    [Header("Rewired Input")]
    public int rewiredPlayerID;
    private Player rewiredPlayer;

    [Header("Events")] 
    public Action OnPauseGame;
    public Action OnRun;
    public Action OnStopRun;
    public Action OnJump;
    public Action OnJumpButtonUp;
    public Action OnClimb;
    public Action OnClimbing;
    public Action OnStopClimb;
    public Action OnAttachRope;
    public Action OnExtendRope;
    public Action OnShortenRope;

    private void Start()
    {
        rewiredPlayer = ReInput.players.GetPlayer(rewiredPlayerID);
    }

    private void Update()
    {
        //if its online multiplayer and not the owner, dont do shit
        if(NetworkController.Instance.onlineMultiplayer && !IsOwner) return;
        
        //menu and camera (parallax Input) always tracked for menu parallax effect and controls
        menuInput = rewiredPlayer.GetAxis("UIVertical");
        cameraInput = new Vector2(rewiredPlayer.GetAxis("cameraHorizontal"), rewiredPlayer.GetAxis("cameraVertical"));

        
        //Only track inputs when PlayerInputs Enabled(not while main Menu)
        //game can be paused whenever (is excepted in gameManager when main menu anyway)
        if (rewiredPlayer.GetButtonDown("pauseGame"))
        {
            OnPauseGame.Invoke();
        }
        
        if (inputEnabled)
        {
            movementInput = new Vector2(rewiredPlayer.GetAxis("moveHorizontal"), rewiredPlayer.GetAxis("moveVertical"));

            if (rewiredPlayer.GetButtonDown("climb"))
            {
                OnClimb.Invoke();
            }

            if (rewiredPlayer.GetButton("climb"))
            {
                OnClimbing.Invoke();
            }
            
            if (rewiredPlayer.GetButtonUp("climb"))
            {
                OnStopClimb.Invoke();
            }
            
            if (rewiredPlayer.GetButton("run"))
            {
                OnRun.Invoke();
            }
            
            if (rewiredPlayer.GetButtonUp("run"))
            {
                OnStopRun.Invoke();
            }
            
            if (rewiredPlayer.GetButtonDown("jump"))
            {
                OnJump.Invoke();
            }

            if (rewiredPlayer.GetButtonUp("jump"))
            {
                OnJumpButtonUp.Invoke();
            }

            if (rewiredPlayer.GetButtonDown("attachRope"))
            {
                OnAttachRope.Invoke();
            }

            if (rewiredPlayer.GetButtonDown("extendRope"))
            {
                OnExtendRope.Invoke();
            }
            
            if (rewiredPlayer.GetButtonDown("shortenRope"))
            {
                OnShortenRope.Invoke();
            }
        }
    }

    public Vector2 GetMovementInput()
    {
        return movementInput;
    }

    public void ResetMovementInput()
    {
        movementInput = Vector2.zero;
    }

    public Vector2 GetCameraInput()
    {
        return cameraInput;
    }

    public float GetMenuInput()
    {
        return menuInput;
    }
    
    public void SetPlayerInputEnabled(bool setter)
    {
        inputEnabled = setter;
        if(!setter)
            ResetMovementInput();
    }

    public bool GetPlayerInputEnabled()
    {
        return inputEnabled;
    }

    public void TogglePlayerInputEnabled()
    {
        inputEnabled = !inputEnabled;
    }
}
