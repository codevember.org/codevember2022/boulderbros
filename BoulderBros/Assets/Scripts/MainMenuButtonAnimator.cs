using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine;
using Unity.VisualScripting;
using UnityEngine.UI;

public class MainMenuButtonAnimator : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, ISelectHandler, IDeselectHandler
{
    [SerializeField] private Vector3 initialPos;
    [SerializeField] private float highLightPos;
    [SerializeField] private Image buttonImage;
    [SerializeField] private Vector4 extraButtonPadding;
    [SerializeField] private bool initialized;

    private void Start()
    {
        initialPos = transform.position;
        buttonImage = GetComponent<Image>();
        initialized = true;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        transform.position = new Vector3(initialPos.x - highLightPos, initialPos.y, initialPos.z);
        buttonImage.raycastPadding = extraButtonPadding;
        Debug.Log("should move button");
    }

    public void OnSelect(BaseEventData eventData)
    {
        if (initialized)
        {
            transform.position = new Vector3(initialPos.x - highLightPos, initialPos.y, initialPos.z);
            buttonImage.raycastPadding = extraButtonPadding;
            Debug.Log("button being selected");
        }
    }

    public void OnDeselect(BaseEventData eventData)
    {
        Debug.Log("button being deselected");
        transform.position = initialPos;
        buttonImage.raycastPadding = Vector4.zero;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        transform.position = initialPos;
        buttonImage.raycastPadding = Vector4.zero;
    }
}
