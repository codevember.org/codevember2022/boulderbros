using System;
using UnityEngine;
using Unity.Netcode;
using Unity.VisualScripting;
using UnityEngine.Serialization;

public class GroundCheck : NetworkBehaviour
{
    [Header("Collision Checks")] 
    [SerializeField] private PlayerController playerController;
    [SerializeField] private PlayerAnimator playerAnimator;
    [SerializeField] private Collider2D playerCollider2D;
    [SerializeField] private Rigidbody2D playerRigidbody2D;
    [SerializeField] private GroundType groundType;
    [SerializeField] private float currentGroundAngle;
    [SerializeField] private float groundCheckDistance = 0.25f;
    [SerializeField] private Vector2 groundNormal;
    [SerializeField] private LayerMask groundLayerMask;                          
    [SerializeField] private ContactFilter2D contactFilter;
    
    [SerializeField] private float fallDamageThreshold;
    
    public enum GroundType
    {
        untagged,
        grass,
        wood,
        stone,
        snow,
        ice
    }

    public Action OnGroundTypeChanged;

    private void FixedUpdate()
    {
        //if its online multiplayer and not the owner, dont do shit
        if(NetworkController.Instance.onlineMultiplayer && !IsOwner) return;
        
        IsGrounded();
    }

    private void OnDrawGizmos()
    {
        float radius = playerCollider2D.bounds.size.x /2f;
        Vector3 circleCastOrigin = new Vector3(transform.position.x, transform.position.y - groundCheckDistance, transform.position.z);
        
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(circleCastOrigin, radius);
    }

    private void IsGrounded()
    {
        ResetGroundcheck();
        
        RaycastHit2D hitDown = Physics2D.CircleCast(transform.position, playerCollider2D.bounds.size.x /2f, transform.up * -1, groundCheckDistance, groundLayerMask);

        //friction depending on normal of ground
        //SetFriction(hitDown);

        if (hitDown)
        {
            //Debug.Log("Hit ground with a velocity of: " + -rigidBody2D.velocity.y + " " + player);
            currentGroundAngle = Vector3.Angle(hitDown.normal, Vector3.up);
            SetCurrentGroundType(hitDown.collider.gameObject);
            
            if (currentGroundAngle <= 60)
            {
                playerController.isGrounded = true;
                playerController.isInAir = false;
                playerController.isFalling = false;
                //On is Grounded again check for fall damage
                FallDamage();
            }
            else
            {
                playerController.isGrounded = false;
                CheckAirState();
            }
        }
        else
        {
            CheckAirState();
        }
        
        playerAnimator.SetGroundedAnimation(playerController.isGrounded);
    }

    private void ResetGroundcheck()
    {
        playerController.isGrounded = false;
    }

    private void SetCurrentGroundType(GameObject go)
    {
        GroundType oldGroundType = groundType;
        
        var tagged = go.tag;
        switch (tagged)
        {
            case ("Untagged"):
                groundType = GroundType.untagged;
                break;
            
            case "grass":
                groundType = GroundType.grass;
                break;
            
            case "wood":
                groundType = GroundType.wood;
                break;
            
            case "stone":
                groundType = GroundType.stone;
                break;
            
            case "snow":
                groundType = GroundType.snow;
                break;
            
            case "ice":
                groundType = GroundType.ice;
                break;
        }
        GroundTypeChanged(oldGroundType);
    }

    private void GroundTypeChanged(GroundType oldGroundType)
    {
        if (groundType != oldGroundType)
        {
            Debug.Log("Ground Type Changed");
            OnGroundTypeChanged.Invoke();
        }
    }

    public void SetWallGroundType()
    {
        SetCurrentGroundType(playerController.GetWallCheck().GetClosestWallObject());
    }
    
    public GroundType GetGroundType()
    {
        return groundType;
    }
    private void CheckAirState()
    {
        if (!playerController.isClimbing)
        {
            playerController.isInAir = true;
            if (playerRigidbody2D.linearVelocity.y < -0.05f)
            {
                playerController.isFalling = true;
            }
            else
            {
                playerController.isFalling = false;
            }
        }
        else
        {
            playerController.isInAir = false;
            playerController.isFalling = false;
        }
    }
    private void FallDamage()
    {
        Rigidbody2D rigid = playerRigidbody2D;
        if (-rigid.linearVelocity.y >= fallDamageThreshold)
        {
            Debug.Log(playerController + " hit ground with a velocity of: " + -rigid.linearVelocity);
            // playerController.GetPlayerAnimator().SetDeathAnimation(true);
            // playerController.GetPlayerAnimator().SetFaceExpression("hurt");
            // GameManager.Instance.GetPlayerManager().SetPlayerAlive(playerController, false);
            // GameManager.Instance.GameOver();
        }
    }

    public LayerMask GetGroundLayerMask()
    {
        return groundLayerMask;
    }

    public Vector2 GetGroundNormal()
    {
        return groundNormal;
    }
}
