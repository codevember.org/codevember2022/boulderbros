//using System.Collections;
//using System.Collections.Generic;
using UnityEngine;

public enum CameraFollowType
{
    followConstant,
    followPoints
}

public class CameraManager : MonoBehaviour
{
    [SerializeField] private CameraFollowPoints cameraFollowPoints;
    [SerializeField] private CameraFollowConstant cameraFollowConstant;
    [SerializeField] private Transform cameraMidPoint;
    [SerializeField] private Transform player1Transform;
    [SerializeField] private Transform player2Transform;
    [SerializeField] private CameraFollowType cameraFollowType;

    public static CameraManager Instance { get; private set; }

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Update()
    {
        UpdateMidPoint();
    }

    public void SwitchCameraFollowMode()
    {
        cameraFollowPoints.ResetCurrentPosition();
        cameraFollowPoints.enabled = !cameraFollowPoints.enabled;
        cameraFollowConstant.enabled = !cameraFollowConstant.enabled;
    }

    public void SetCameraFollowMode(CameraFollowType type)
    {
        switch (type)
        {
            case CameraFollowType.followConstant:
                cameraFollowType = CameraFollowType.followConstant;
                cameraFollowPoints.enabled = false;
                cameraFollowConstant.enabled = true;
                break;
            case CameraFollowType.followPoints:
                cameraFollowType = CameraFollowType.followPoints;
                cameraFollowConstant.enabled = false;
                cameraFollowPoints.enabled = true;
                break;
        }
    }

    private void CheckCameraFollowMode()
    {
        if (cameraFollowPoints.enabled)
            cameraFollowType = CameraFollowType.followPoints;
        if(cameraFollowConstant.enabled)
            cameraFollowType = CameraFollowType.followConstant;
    }
    
    public CameraFollowType GetCameraFollowType()
    {
        CheckCameraFollowMode();
        return cameraFollowType;
    }

    public void CheckCameraPosition(float triggerX)
    {
        if (cameraMidPoint.position.x >= triggerX)
        {
            SwitchCameraFollowMode();
        }
        if (cameraMidPoint.position.x < triggerX)
        {
            if (cameraFollowConstant.enabled)
            {
                SwitchCameraFollowMode();
            }
        }
    }

    private void UpdateMidPoint()
    {
        cameraMidPoint.position = Vector3.Lerp(player1Transform.position, player2Transform.position, 0.5f);
    }

    public Vector3 GetMidPointPosition() 
    { 
        return cameraMidPoint.position; 
    }

    public CameraFollowConstant GetCameraFollowConstant()
    {
        return cameraFollowConstant;
    }

    public CameraFollowPoints GetCameraFollowPoints()
    {
        return cameraFollowPoints;
    }

    
}
