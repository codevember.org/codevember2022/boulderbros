using System.Collections;
//using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.Rendering;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using UnityEngine.UIElements;

public class UIManager : MonoBehaviour
{
    public static UIManager Instance { get; private set; }
    
    [Header("Reference/Managers")]
    [SerializeField] private PlayerController playerController1;
    [SerializeField] private PlayerController playerController2;
    [SerializeField] private ButtonManager buttonManager;
    [SerializeField] private SceneTransitionManager sceneTransitionManager;

    [Header("MainMenu UI")]
    [SerializeField] private GameObject mainMenuUI;
    [SerializeField] private GameObject mainMenuEnvironment;
    [SerializeField] private GameObject initialSelectedMainMenuGo;

    [Header("Pause UI")]
    [SerializeField] private GameObject pauseUI;
    [SerializeField] private GameObject pauseUIRopeMenu;
    [SerializeField] private GameObject initialSelectedPauseGo;
    [SerializeField] private Volume blurVolume;
    [SerializeField] private Animator pauseUIAnimator;
    [SerializeField] private AnimationClip uiRopeInAnim;
    [SerializeField] private AnimationClip uiRopeOutAnim;

    [Header("GameOver UI")]
    [SerializeField] private GameObject gameOverScreen;
    [SerializeField] private TextMeshProUGUI gameOverText;
    
    [Header("Ingame UI")]
    [SerializeField] private GameObject ingameEnvironment;
    [SerializeField] private GameObject[] ingameUI;

    [Header("Ingame UI/Stamina")] 
    [SerializeField] private Material staminaMatP1;
    [SerializeField] private Material staminaMatP2;
    [SerializeField] private SpriteRenderer staminaCircleP1;
    [SerializeField] private SpriteRenderer staminaCircleP2;
    
    [Header("Ingame UI/Stamina/Exhaust")]
    [SerializeField] private Gradient exhaustGradient;
    
    [Header("Ingame UI/Height")]
    [SerializeField] private TextMeshProUGUI totalHeightText;
    [SerializeField] private TextMeshProUGUI fallHeightText;
    [SerializeField] private UnityEngine.UI.Image fallHeightDangerIcon;
    [SerializeField] private Color currentFallColor;
    [SerializeField] private Color defaultFallColor;
    [SerializeField] private Color dangerFallColor;

    [Header("Ingame UI/Speech")]
    [SerializeField] private GameObject speechBubbleP1;
    [SerializeField] private GameObject speechBubbleP2;
    [SerializeField] private GameObject speechBubbleIconP1;
    [SerializeField] private GameObject speechBubbleIconP2;
    [SerializeField] private TextMeshProUGUI speechBubbleTextP1;
    [SerializeField] private TextMeshProUGUI speechBubbleTextP2;
    [SerializeField] private float speechTime = 2.5f;

    [Header("Ingame UI/Speech/Height")]
    [SerializeField] private string[] heightSentences;
    [SerializeField] private bool heightWarningTriggeredP1;
    [SerializeField] private bool heightWarningTriggeredP2;
    [SerializeField] private float heightWarningCooldown;
    [SerializeField] private float heightWarningTimerP1;
    [SerializeField] private float heightWarningTimerP2;

    [Header("Ingame UI/Speech/Hint")]
    [SerializeField] private string mountainHint;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        ToggleMainMenuUI(MenuManager.Instance.GetMainMenuEnabled());
        ToggleIngameUI(MenuManager.Instance.GetIngameUIEnabled());
        ResetStaminaUI(staminaMatP1, playerController1);
        ResetStaminaUI(staminaMatP2, playerController2);  
    }

    private void Update()
    {
        RotateStaminaCircle(playerController1);
        RotateStaminaCircle(playerController2);
        RotateSpeechBubble(playerController1);
        RotateSpeechBubble(playerController2);
        UpdateTotalHeight();
        UpdateFallHeight();
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        UpdateStaminaUI(staminaMatP1, playerController1);
        UpdateStaminaUI(staminaMatP2, playerController2);
        HeightWarningOnCooldown(playerController1);
        HeightWarningOnCooldown(playerController2);
    }

    //if main menu check for input type in case controller is attached and no button selected (no controls)
    public void ToggleMainMenuUI(bool toggle)
    {
        mainMenuUI.SetActive(toggle);
        
        if (toggle)
            StartCoroutine(CheckSelectionInput());
        else
            ResetSelectedEventButton();
    }

    public void ToggleMainMenuEnvironment(bool toggle)
    {
        mainMenuEnvironment.SetActive(toggle);
    }

    public void ToggleIngameUI(bool toggle)
    {
        foreach (GameObject go in ingameUI)
        {
            go.SetActive(toggle);
        } 
    }

    public void ToggleIngameEnvironment(bool toggle)
    {
        ingameEnvironment.SetActive(toggle);
    }

    private void SetFirstNavigationUIElement(GameObject toSelect)
    {
        if (GameManager.Instance.GetInputController().GetJoystickConnected())
            SetInitialSelectedEventButton(toSelect);
        else
            ResetSelectedEventButton();
    }

    private IEnumerator CheckSelectionInput()
    {
        while(pauseUI.activeSelf || mainMenuUI.activeSelf)
        {
            CheckInput();
            yield return new WaitForSecondsRealtime(1f);
        }  
    }
    private void CheckInput()
    {
        //Debug.Log("Should check Input type while: " + EventSystem.current.currentSelectedGameObject);
        if (pauseUI.activeSelf && EventSystem.current.currentSelectedGameObject == null)
        {
            SetFirstNavigationUIElement(initialSelectedPauseGo);
        }
        if (mainMenuUI.activeSelf && EventSystem.current.currentSelectedGameObject == null)
        {
            SetFirstNavigationUIElement(initialSelectedMainMenuGo);
        }
    }

    public void TogglePauseUI()
    {  
        bool paused = pauseUI.activeSelf;
        if (!paused)
        {
            pauseUI.SetActive(true);
            GameManager.Instance.GetPlayerManager().SetPlayersInputEnabled(false);
            ToggleIngameUI(false);
            StartCoroutine(CheckSelectionInput());
            TogglePauseRopeAnimation(true);
            StartCoroutine(ToggleUIRopeIn(true)); 
        }
        else
        {
            ResetSelectedEventButton();
            TogglePauseRopeAnimation(false);
            StartCoroutine(ToggleUIRopeOut(true));
        }  
    }

    private void SetInitialSelectedEventButton(GameObject go)
    {
        EventSystem.current.SetSelectedGameObject(go);
    }

    private void ResetSelectedEventButton()
    {
        EventSystem.current.SetSelectedGameObject(null);
    }

    public void TogglePauseRopeAnimation(bool toggle)
    {
        if(toggle)
        {
            pauseUIAnimator.SetBool("ropeOut", false);
            pauseUIAnimator.SetBool("ropeIn", true);   
        }
        else
        {
            pauseUIAnimator.SetBool("ropeIn", false);
            pauseUIAnimator.SetBool("ropeOut", true);
        }
    }

    private IEnumerator ToggleUIRopeIn(bool timeFreeze)
    {
        yield return new WaitForSeconds(uiRopeInAnim.length);
        if(timeFreeze)
            GameManager.Instance.ToggleTimeFreeze();
    }

    private IEnumerator ToggleUIRopeOut(bool timeFreeze)
    {
        if (timeFreeze)
            GameManager.Instance.ToggleTimeFreeze();
        yield return new WaitForSeconds(0.7f);
        
        ToggleIngameUI(true);
        pauseUI.SetActive(false);
        GameManager.Instance.GetPlayerManager().SetPlayersInputEnabled(true);
    }

    public void SetActiveIngameUI(PlayerController controller, bool value)
    {
        if(controller == playerController1)
            staminaCircleP1.gameObject.SetActive(value);
        if (controller == playerController2)
            staminaCircleP2.gameObject.SetActive(value);
    }

    public void SetActiveIngameUI(bool value)
    {
        staminaCircleP1.gameObject.SetActive(value);
        staminaCircleP2.gameObject.SetActive(value);
    }

    public void SetActiveScreen(GameObject screen, bool value)
    {
        screen.SetActive(value);
    }

    public void GameOverScreen()
    {
        SetActiveIngameUI(playerController1, false);
        SetActiveIngameUI(playerController2, false);
        SetGameOverText();
        gameOverScreen.SetActive(true);
    }

    public void StartGameUI()
    {
        ToggleIngameUI(MenuManager.Instance.GetIngameUIEnabled());
        ToggleMainMenuUI(MenuManager.Instance.GetMainMenuEnabled());
        playerController1.GetPlayerMovement().Flip();
        playerController2.GetPlayerMovement().Flip();
        
    }

    public void RestartGameUI()
    {
        DeactivateGameOverScreen();   
        ToggleIngameUI(MenuManager.Instance.GetIngameUIEnabled());
        ToggleMainMenuUI(MenuManager.Instance.GetMainMenuEnabled());
        ResetGameOverText();
        ResetStaminaUI();
    }

    private void DeactivateGameOverScreen()
    {
        gameOverScreen.SetActive(false);
    }

    private void ResetGameOverText()
    {
        gameOverText.text = "<color=red>Game Over</color>";
    }
    public void SetGameOverText()
    {
        bool aliveP1 = GameManager.Instance.GetPlayerManager().GetPlayerAlive(playerController1);
        bool aliveP2 = GameManager.Instance.GetPlayerManager().GetPlayerAlive(playerController2);

        if (!aliveP1 || !aliveP2)
        {
            if (!aliveP1 && !aliveP2)
            {
                gameOverText.text = gameOverText.text + "\n <size=80%> <color=orange>Garry</color> and <color=blue>Jimmy</color> died.";
            }
            else if (!aliveP1 && aliveP2)
            {
                gameOverText.text = gameOverText.text + "\n <size=80%><color=orange>Garry</color> died.";
            }
            else if (aliveP1 && !aliveP2)
            {
                gameOverText.text = gameOverText.text + "\n <size=80%><color=blue>Jimmy</color> died.";
            }
        }
    }

    

    #region Height Indicators
    private void UpdateTotalHeight()
    {
        totalHeightText.text = GameManager.Instance.GetHeightManager().GetCurrentTotalHeight().ToString()+ "m";
    }

    private void UpdateFallHeight()
    {
        fallHeightText.text = GameManager.Instance.GetHeightManager().GetCurrentFallHeight() + "m";
    }

    public void SetFallColor(bool danger)
    {
        if (danger)
        {
            fallHeightDangerIcon.color = fallHeightText.color = dangerFallColor;
            
        }   
        else
        {
            fallHeightDangerIcon.color = fallHeightText.color = defaultFallColor;
        }
            
    }
    #endregion


    #region SpeechBubble
    public void FlipSpeechBubble(PlayerController controller)
    {
        if (controller == playerController1)
        {
            speechBubbleP1.transform.localScale = new Vector3(speechBubbleP1.transform.localScale.x * -1f, speechBubbleP1.transform.localScale.y, speechBubbleP1.transform.localScale.z);
        }
        if (controller == playerController2)
        {
            speechBubbleP2.transform.localScale = new Vector3(speechBubbleP2.transform.localScale.x * -1f, speechBubbleP2.transform.localScale.y, speechBubbleP2.transform.localScale.z);
        }
    }

    private void RotateSpeechBubble(PlayerController controller)
    {
        if (controller == playerController1)
        {
            speechBubbleP1.transform.localEulerAngles = new Vector3(speechBubbleP1.transform.localEulerAngles.x, speechBubbleP1.transform.localEulerAngles.y, -controller.transform.localEulerAngles.z);
        }

        if (controller == playerController2)
        {
            speechBubbleP2.transform.localEulerAngles = new Vector3(speechBubbleP2.transform.localEulerAngles.x, speechBubbleP2.transform.localEulerAngles.y, -controller.transform.localEulerAngles.z);
        }
    }

    public void TriggerHint(PlayerController controller, string hint)
    {
        if (controller == playerController1)
        {
            speechBubbleTextP1.text = hint;
            speechBubbleP1.SetActive(true);
            StartCoroutine(DeactivateSpeechBubble(playerController1));
        }
        if (controller == playerController2)
        {
            speechBubbleTextP2.text = hint;
            speechBubbleP2.SetActive(true);
            StartCoroutine(DeactivateSpeechBubble(playerController2));
        }
    }

    private void TriggerHeightSpeech(PlayerController controller)
    {
        if (controller == playerController1)
        {
            int randomIndex = Random.Range(0, heightSentences.Length);
            speechBubbleTextP1.text = heightSentences[randomIndex];
            speechBubbleP1.SetActive(true);
            StartCoroutine(DeactivateSpeechBubble(playerController1));
        }
        if (controller == playerController2)
        {
            int randomIndex = Random.Range(0, heightSentences.Length);
            speechBubbleTextP2.text = heightSentences[randomIndex];
            speechBubbleP2.SetActive(true);
            StartCoroutine(DeactivateSpeechBubble(playerController2));
        }
    }

    private void HeightWarningOnCooldown(PlayerController controller)
    {
        if (controller == playerController1)
        {
            if (heightWarningTriggeredP1 && heightWarningTimerP1 > 0)
            {
                heightWarningTimerP1 -= Time.deltaTime;
                //Debug.Log("is decreasing " + heightWarningTimerP1.ToString() + " " + heightWarningTimerP1);
            }

            if (heightWarningTimerP1 < 0)
                heightWarningTimerP1 = 0;

            if (heightWarningTimerP1 == 0)
            {
                heightWarningTriggeredP1 = false;
            }
        }

        if (controller == playerController2)
        {
            if (heightWarningTriggeredP2 && heightWarningTimerP2 > 0)
            {
                heightWarningTimerP2 -= Time.deltaTime;
                //Debug.Log("is decreasing " + heightWarningTimerP2.ToString() + " " + heightWarningTimerP2);
            }

            if (heightWarningTimerP2 < 0)
                heightWarningTimerP2 = 0;

            if (heightWarningTimerP2 == 0)
            {
                heightWarningTriggeredP2 = false;
            }
        }
    }

    public void TriggerHeightWarning(PlayerController controller, bool value)
    {
        if (controller == playerController1)
        {
            if (value && !heightWarningTriggeredP1 && !controller.heightWarned && !playerController2.heightWarned)
            {
                TriggerHeightSpeech(controller);
                heightWarningTriggeredP1 = true;
                heightWarningTimerP1 = heightWarningCooldown;
            }
        }
        if (controller == playerController2)
        {
            if (value && !heightWarningTriggeredP2 && !controller.heightWarned && !playerController1.heightWarned)
            {
                TriggerHeightSpeech(controller);
                heightWarningTriggeredP2 = true;
                heightWarningTimerP2 = heightWarningCooldown;
            }
        }
    }


    private IEnumerator DeactivateSpeechBubble(PlayerController controller)
    {
        yield return new WaitForSeconds(speechTime);
        if (controller == playerController1)
        {
            speechBubbleP1.SetActive(false);
        }
        if (controller == playerController2)
        {
            speechBubbleP2.SetActive(false);
        }
    }
    #endregion
    #region Stamina UI
    public void FlipStaminaCircle(PlayerController controller)
    {
        if (controller == playerController1)
        {
            staminaCircleP1.transform.localPosition = new Vector3(-staminaCircleP1.transform.localPosition.x, staminaCircleP1.transform.localPosition.y, staminaCircleP1.transform.localPosition.z);
            staminaCircleP1.flipX = !staminaCircleP1.flipX;
        }
        if (controller == playerController2)
        {
            staminaCircleP2.transform.localPosition = new Vector3(-staminaCircleP2.transform.localPosition.x, staminaCircleP2.transform.localPosition.y, staminaCircleP2.transform.localPosition.z);
            staminaCircleP2.flipX = !staminaCircleP2.flipX;
        }
    }

    private void RotateStaminaCircle(PlayerController controller)
    {
        if (controller == playerController1)
        {
            staminaCircleP1.transform.localEulerAngles = new Vector3(staminaCircleP1.transform.localEulerAngles.x, staminaCircleP1.transform.localEulerAngles.y, -controller.transform.localEulerAngles.z);
        }

        if (controller == playerController2)
        {
            staminaCircleP2.transform.localEulerAngles = new Vector3(staminaCircleP2.transform.localEulerAngles.x, staminaCircleP2.transform.localEulerAngles.y, -controller.transform.localEulerAngles.z);
        }
    }

    public void UpdateStaminaUI(Material mat, PlayerController controller)
    {
        float stamina = controller.GetPlayerStats().GetCurrentStamina();
        float maxStamina = controller.GetPlayerStats().GetMaxStamina();
        if (stamina != maxStamina)
        {
            float currentStamina = maxStamina - stamina;
            mat.SetFloat("_RemoveSegments", currentStamina);
            ColorStaminaUI(mat, controller);
        }
    }

    private void ColorStaminaUI(Material mat, PlayerController controller)
    {
        mat.color = exhaustGradient.Evaluate(controller.GetPlayerStats().GetCurrentStamina()/100f);
    }

    public void ResetStaminaUI(Material mat, PlayerController controller)
    {
        mat.SetFloat("_RemoveSegments", 0);
        ColorStaminaUI(mat, controller);
    }

    public void ResetStaminaUI()
    {
        staminaMatP1.SetFloat("_RemoveSegments", 0);
        ColorStaminaUI(staminaMatP1, playerController1);
        staminaMatP2.SetFloat("_RemoveSegments", 0);
        ColorStaminaUI(staminaMatP2, playerController2);
    }
    #endregion

    public ButtonManager GetButtonManager()
    {
        return buttonManager;
    }
    
    public SceneTransitionManager GetSceneTransitionManager()
    {
        return sceneTransitionManager;
    }
}
