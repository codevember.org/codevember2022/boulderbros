using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VolumeTrigger : MonoBehaviour
{
    public BoxCollider2D col;
    public Vector3 colVector;
    public Color gizmoColor;
    public bool player1Triggered = false;
    public bool player2Triggered = false;

    // Start is called before the first frame update
    void Start()
    {
        InitializeColVector();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void InitializeColVector()
    {
        colVector = new Vector3(col.size.x, col.size.y, 0);
    }

    public void OnValidate()
    {
        InitializeColVector();
    }
    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("CameraTarget"))
        {
            Debug.Log(other);
            if (other.gameObject.name == "CameraTargetP1")
                player1Triggered = true;
            if (other.gameObject.name == "CameraTargetP2")
                player2Triggered = true;
        }
    }

    public void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("CameraTarget"))
        {
            if (other.gameObject.name == "CameraTargetP1")
                player1Triggered = false;
            if (other.gameObject.name == "CameraTargetP2")
                player2Triggered = false;
        }
    }

    private void OnDrawGizmos()
    {
        Vector3 colOffset = new Vector3(transform.position.x, transform.position.y + col.offset.y, transform.position.z);
        // Draw a blue cube at the transform position
        var color = Gizmos.color;
        Gizmos.color = gizmoColor;
        Gizmos.DrawWireCube(colOffset, colVector);
        Gizmos.color = color;
    }
}
