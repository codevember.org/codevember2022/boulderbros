using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class TutorialVolumeTrigger : VolumeTrigger
{
    [SerializeField] private bool bothPlayersTriggered = false;
    [SerializeField] private GameObject tutorialTooltip;
    [SerializeField] private bool tutorialActivated = false;

    private void Update()
    {
        if ((bothPlayersTriggered||player1Triggered||player2Triggered) && !tutorialActivated)
        {
            tutorialTooltip.SetActive(true);
            Debug.Log("Tutorial should be activated: " + tutorialTooltip);
            tutorialActivated = true;
        }            
    }
}
