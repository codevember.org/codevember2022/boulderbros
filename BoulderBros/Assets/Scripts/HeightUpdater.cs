// using System.Collections;
// using System.Collections.Generic;

using System;
using UnityEngine;

public class HeightUpdater : MonoBehaviour
{
    [Header("References")]
    [SerializeField]private PlayerController playerController1;
    [SerializeField]private PlayerController playerController2;
    
    [Header("Stats")]
    [SerializeField] private float currentTotalHeight;
    [SerializeField] private float currentFallHeight;
    [SerializeField] private float initialPlayerHeightOffset;

    private void Start()
    {
        initialPlayerHeightOffset = Mathf.Abs(CameraManager.Instance.GetMidPointPosition().y);
    }

    private void Update()
    {
        UpdateCurrentTotalHeight();
        UpdateCurrentFallHeight();
    }

    private void UpdateCurrentTotalHeight()
    {
        currentTotalHeight = Mathf.Round(Mathf.Max(playerController1.transform.position.y, playerController2.transform.position.y) + initialPlayerHeightOffset);
    }

    public void ResetInitialHeightOffset()
    {
        initialPlayerHeightOffset = Mathf.Abs(playerController1.transform.position.y + playerController2.transform.position.y)/2;
    }

    private void UpdateCurrentFallHeight()
    {
        currentFallHeight = Mathf.Round(Mathf.Max(playerController1.GetPlayerStats().GetCurrentHeight(), playerController2.GetPlayerStats().GetCurrentHeight()));
    }

    public int GetCurrentTotalHeight()
    {
        return (int)currentTotalHeight;
    }
    
    public int GetCurrentFallHeight()
    {
        return (int)currentFallHeight;
    }
}
