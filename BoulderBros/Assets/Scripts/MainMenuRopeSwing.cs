using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class MainMenuRopeSwing : MonoBehaviour
{
    [SerializeField] private Rigidbody2D jimmyMainMenuRigid2D;
    [SerializeField] private Vector2 swingForce;

    private void Start()
    {
        AddSwingForce();
    }

    private void AddSwingForce()
    {
        jimmyMainMenuRigid2D.AddForce(swingForce, ForceMode2D.Force);
    }
}
