using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CameraVolumeTrigger : VolumeTrigger
{
    public int triggerIndex;
    private void FixedUpdate()
    {
        if (player1Triggered && player2Triggered)
            CameraManager.Instance.GetCameraFollowPoints().PlayerTriggeredCamVolume(triggerIndex);
    }

    private void OnDrawGizmos()
    {
        // Draw a blue cube at the transform position
        var color = Gizmos.color;
        Gizmos.color = Color.blue;
        Gizmos.DrawWireCube(transform.position, colVector);
        Gizmos.color = color;
    } 
}
