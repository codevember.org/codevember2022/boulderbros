using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialImageTooltip : TutorialTooltip
{
    [SerializeField] private SpriteRenderer tutorialImageRenderer;
    [SerializeField] private Sprite keyboardImage;
    [SerializeField] private Sprite xboxGamepadImage;
    [SerializeField] private Sprite psGamepadImage;


    private void SetTutorialImage(Sprite sprite)
    {
        tutorialImageRenderer.sprite = sprite;
    }


    //This will be called when object is enabled.
    void OnEnable()
    {
        Debug.Log("OnEnabled()", gameObject);

        CheckInputDevice();

        if (keyboardActive)
            SetTutorialImage(keyboardImage);
        if (xboxGamepadActive)
            SetTutorialImage(xboxGamepadImage);
        if (psGamepadActive)
            SetTutorialImage(psGamepadImage);

        Fade(tutorialImageRenderer, fadeDuration, 0f);
    }
}
