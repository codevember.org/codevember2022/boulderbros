//using System.Collections;
//using System.Collections.Generic;
using UnityEngine;

public class CameraFollowConstant : MonoBehaviour
{
    [SerializeField]private Vector3 initialPosition;
    [SerializeField]private Transform midPoint;
    //[SerializeField]private Vector3 offset = new Vector3(0, 2, -10);
    [SerializeField]private float smoothTime = 0.25f;

    private void Start()
    {
        initialPosition = transform.position;
    }
    private void LateUpdate()
    {
        midPoint.position = CameraManager.Instance.GetMidPointPosition();
        transform.position = Vector3.Lerp(transform.position, midPoint.position, smoothTime * Time.deltaTime);
        transform.position = new Vector3(transform.position.x, initialPosition.y, initialPosition.z);
    }
}

