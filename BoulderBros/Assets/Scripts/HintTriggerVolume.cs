//using System.Collections;
//using System.Collections.Generic;
using UnityEngine;

public class HintTriggerVolume : VolumeTrigger
{
    [SerializeField] private string hintText;
    //TriggerHint on otherPlayer because he tells the hint to his Bro
    public new void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer != LayerMask.NameToLayer("CameraTarget")) return;
        Debug.Log(other);
        switch (other.gameObject.name)
        {
            case "CameraTargetP1":
                UIManager.Instance.TriggerHint(GameManager.Instance.playerController2, hintText);
                break;
            case "CameraTargetP2":
                UIManager.Instance.TriggerHint(GameManager.Instance.playerController1, hintText);
                break;
        }
    }
}
